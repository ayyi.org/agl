/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2021-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#pragma once

#include "glib.h"
#include "agl/behaviour.h"

typedef struct {
   AGlBehaviour behaviour;
} FullsizeBehaviour;

AGlBehaviourClass* fullsize_get_class ();

AGlBehaviour* fullsize ();

