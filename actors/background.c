/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2012-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#undef USE_GTK
#include <sys/time.h>
#include "agl/actor.h"
#include "agl/shader.h"
#include "debug/debug.h"
#include "actors/texture.h"
#include "actors/background.h"

static AGl* agl = NULL;

static AGlActorClass actor_class = {0, "Background", (AGlActorNew*)background_actor, texture_free};


static void
agl_load_alphamap (char* buf, guint texture, int width, int height)
{
	#define pixel_format GL_ALPHA

	agl_use_texture (texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, width, height, 0, pixel_format, GL_UNSIGNED_BYTE, buf);
#ifdef DEBUG
	gl_warn("binding bg texture");
#endif
}


/*
 *  Create an alpha-map gradient texture
 */
static void
create_background (AGlActor* a)
{
	AGlTextureNode* ta = (AGlTextureNode*)a;

	if (ta->texture) return;

	int width = 8;
	int height = 8;

	guchar* pbuf = g_new0(guchar, width * height);
#if 1
	int y; for (y=0;y<height;y++) {
		for(int x=0;x<width;x++) {
			// with coefficient 2.5, gradient does not go fully transparent
			*(pbuf + y * width + x) = ((x+y) * 0xff) / ((float)width * 2.5);
		}
	}
	// darken the edges
	y = 0;
	for (int x=0;x<width;x++) {
		*(pbuf + y * width + x) = MIN(0xff, *(pbuf + y * width + x) + 0x13);
	}
	y = height - 1;
	for (int x=0;x<width;x++) {
		*(pbuf + y * width + x) = MIN(0xff, *(pbuf + y * width + x) + 0x13);
	}
#else
	// this gradient is brighter in the middle. It only works for stereo.
	int nc = 2;
	int c; for(c=0;c<nc;c++){
		int top = (height / nc) * c;
		int bot = height / nc;
		int mid = height / (2 * nc);
		int y; for(y=0;y<mid;y++){
			int x; for(x=0;x<width;x++){
				int y_ = top + y;
				int val = 0xff * (1.0 + sinf(((float)(-mid + 2 * y)) / mid));
				*(pbuf + y_ * width + x) = ((val) / 8 + ((x+y_) * 0xff) / (width * 2)) / 2;
				y_ = top + bot - y - 1;
				*(pbuf + (y_) * width + x) = ((val) / 8 + ((x+y_) * 0xff) / (width * 2)) / 2;
			}
		}
	}
#endif

	agl_enable(AGL_ENABLE_BLEND);

	glGenTextures(1, &ta->texture);
	if (glGetError() != GL_NO_ERROR) { perr ("couldnt create bg_texture."); goto out; }

	agl_load_alphamap((char*)pbuf, ta->texture, width, height);

  out:
	g_free(pbuf);
}


static void
bg_actor_set_state (AGlActor* actor)
{
	if (agl->use_shaders) {
#if 0
		((AlphaMapShader*)actor->program)->uniform.fg_colour = 0x4488ffff; // TODO use theme colour, or pass as argument.
#else
		((AlphaMapShader*)actor->program)->uniform.fg_colour = 0x333333ff;
#endif
	} else {
		glColor4f(0.4, 0.4, 0.4, 1.0);
	}
}


static bool
bg_actor_paint (AGlActor* actor)
{
	agl_textured_rect(((AGlTextureNode*)actor)->texture,
		0,
		0,
		agl_actor__width(actor->parent),
		actor->parent->region.y2,
		NULL
	);

	return true;
}


AGlActor*
background_actor (void* view)
{
	agl = agl_get_instance();

	return (AGlActor*)agl_actor__new (AGlTextureNode,
		.actor = {
			.class = &actor_class,
			.region = {
				.x2 = 1, .y2 = 1 // must have size else will not be rendered
			},
			.init = create_background,
			.set_state = bg_actor_set_state,
			.paint = bg_actor_paint,
			.program = (AGlShader*)agl->shaders.alphamap
		}
	);
}
