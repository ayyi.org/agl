/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2021-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 | test/shared_context.c                                                |
 | Demonstrate sharing of texture between two gtk widgets               |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include <getopt.h>
#include <gdk/gdkkeysyms.h>
#include "agl/gtk.h"
#include "agl/shader.h"
#include "agl/behaviours/cache.h"
#include "agl/gtk-area.h"
#include "test/common2.h"
#include "actors/texture.h"
#include "agl/text/text_node.h"

GtkWidget* canvas   = NULL;
AGlActor*  nodes[2] = {0,};
gpointer   tests[]  = {};
static GLuint textures[1] = {0,};

static AGl* agl = NULL;

static void on_canvas_realise (GtkWidget*, gpointer);
static void on_allocate       (GtkWidget*, GtkAllocation*, gpointer);
static void create_texture    ();

static const struct option long_options[] = {
	{ "non-interactive",  0, NULL, 'n' },
};

static const char* const short_options = "n";


int
main (int argc, char* argv[])
{
	agl = agl_get_instance ();
	set_log_handlers ();

	if (g_getenv("NON_INTERACTIVE")) {
		g_timeout_add(3000, (gpointer)exit, NULL);
	}

	int opt;
	while ((opt = getopt_long (argc, argv, short_options, long_options, NULL)) != -1) {
		switch (opt) {
			case 'n':
				g_timeout_add(3000, (gpointer)exit, NULL);
				break;
		}
	}

	gtk_init (&argc, &argv);

	GdkGLConfig* glconfig = NULL;
	if (!(glconfig = gdk_gl_config_new_by_mode (GDK_GL_MODE_RGBA | GDK_GL_MODE_DEPTH | GDK_GL_MODE_DOUBLE))) {
		perr ("Cannot initialise gtkglext.");
		return EXIT_FAILURE;
	}

	GtkWidget* window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

	GtkWidget* vbox = gtk_vbox_new(FALSE, 4);
	gtk_container_add ((GtkContainer*)window, (GtkWidget*)vbox);

	GlArea* canvas1 = gl_area_new ();
	canvas = (GtkWidget*)canvas1;
	gtk_widget_set_size_request (canvas, 280, 110);
	gtk_box_pack_start(GTK_BOX(vbox), (GtkWidget*)canvas1, TRUE, TRUE, 0);

	AGlActor* text = agl_actor__add_child ((AGlActor*)canvas1->scene, text_node (NULL));
	text_node_set_text ((TextNode*)text, "Two Gtk widgets with a shared texture");
	text->region = (AGlfRegion){10, 10, 300, 32};

	int w = 50;
	int h = 50;
	int y = 40;

	nodes[0] = agl_actor__add_child ((AGlActor*)canvas1->scene, texture_node(NULL));
	nodes[0]->region = (AGlfRegion){10, y, 10 + w, y + h};

	y = 20;
	GlArea* canvas2 = gl_area_new ();
	gtk_widget_set_size_request ((GtkWidget*)canvas2, 280, 90);
	gtk_box_pack_start(GTK_BOX(vbox), (GtkWidget*)canvas2, TRUE, TRUE, 0);
	nodes[1] = agl_actor__add_child ((AGlActor*)canvas2->scene, texture_node(NULL));
	nodes[1]->region = (AGlfRegion){10, y, 10 + w, y + h};

	g_signal_connect ((gpointer)canvas1, "realize",       G_CALLBACK(on_canvas_realise), NULL);
	g_signal_connect ((gpointer)canvas1, "size-allocate", G_CALLBACK(on_allocate), NULL);
	g_signal_connect ((gpointer)canvas1, "expose-event",  G_CALLBACK(agl_actor__on_expose), canvas1->scene);

	gtk_widget_show_all (window);

	gboolean key_press (GtkWidget* widget, GdkEventKey* event, gpointer user_data)
	{
		switch (event->keyval) {
			case 113:
				exit(EXIT_SUCCESS);
				break;
			case GDK_Delete:
				break;
			default:
				dbg(0, "%i", event->keyval);
				break;
		}
		return TRUE;
	}

	g_signal_connect (window, "key-press-event", G_CALLBACK(key_press), NULL);

	gboolean window_on_delete (GtkWidget* widget, GdkEvent* event, gpointer user_data)
	{
		gtk_main_quit ();
		return false;
	}
	g_signal_connect (window, "delete-event", G_CALLBACK(window_on_delete), NULL);

	gtk_main ();

	return EXIT_SUCCESS;
}


static void
on_canvas_realise (GtkWidget* _canvas, gpointer user_data)
{
	if (!GTK_WIDGET_REALIZED (canvas))
		return;

	on_allocate (canvas, &canvas->allocation, user_data);

	if (!textures[0]) {
		create_texture ();
		((AGlTextureNode*)nodes[0])->texture = textures[0];
		((AGlTextureNode*)nodes[1])->texture = textures[0];
	}
}


static void
on_allocate (GtkWidget* widget, GtkAllocation* allocation, gpointer user_data)
{
	((AGlActor*)((GlArea*)canvas)->scene)->region.x2 = allocation->width;
	((AGlActor*)((GlArea*)canvas)->scene)->region.y2 = allocation->height;
}


static void
create_texture ()
{
	glGenTextures (1, textures);

	int width = 256;
	int height = 256;
	char* pbuf = g_new0(char, width * height * 3);
	for (int y=0;y<height;y++) {
		for (int x=0;x<width;x++) {
			*(pbuf + y * (width*3) + (x*3))     = ((x+y) * 0xff) / (width * 2);
			*(pbuf + y * (width*3) + (x*3) + 1) = ((x+y) * 0xff) / (width * 2);
			*(pbuf + y * (width*3) + (x*3) + 2) = ((x+y) * 0xff) / (width * 2);
		}
	}

	#define pixel_format2 GL_RGB
	agl_use_texture (textures[0]);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D    (GL_TEXTURE_2D, 0, 3, width, height, 0, pixel_format2, GL_UNSIGNED_BYTE, pbuf);

	g_free (pbuf);
}
