/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2020-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include <math.h>
#undef USE_GTK
#include <getopt.h>
#include "gdk/gdk.h"
#include "debug/debug.h"
#include "agl/ext.h"
#include "agl/x11.h"
#include "agl/debug.h"
#include "agl/shader.h"
#include "agl/text/text_node.h"
#include "actors/debug.h"
#define __glx_test__
#include "test/common2.h"

extern AGlMaterialClass aaline_class;

#define VEC2 2
#define N_LINES 16

static AGl* agl = NULL;
static AGlScene* scene = NULL;

AGlActor* lines_actor (void*);
AGlActor* split_lines_actor (void*);

struct {
	AGlActor* text;
} layers = {0,};

static const struct option long_options[] = {
	{ "non-interactive",  0, NULL, 'n' },
};

static const char* const short_options = "n";


int
main (int argc, char* argv[])
{
	const int width = 400, height = 200;

	if (g_getenv("NON_INTERACTIVE")) {
		g_timeout_add(3000, (gpointer)exit, NULL);
	}

	int opt;
	while ((opt = getopt_long (argc, argv, short_options, long_options, NULL)) != -1) {
		switch (opt) {
			case 'n':
				g_timeout_add(3000, (gpointer)exit, NULL);
				break;
		}
	}

	AGlWindow* window = agl_window("Text test", 0, 0, width, height, 0);
	scene = window->scene;
	XMapWindow(dpy, window->window);

	layers.text = agl_actor__add_child((AGlActor*)scene, text_node(NULL));
	text_node_set_text((TextNode*)layers.text, g_strdup("Lines test"));
	layers.text->colour = 0xffff33ff;
	layers.text->region = (AGlfRegion){.x2 = 80, .y2 = 30};

	AGlActor* lines = agl_actor__add_child((AGlActor*)scene, lines_actor(NULL));
	lines->region = (AGlfRegion){.x1 = 25., .y1 = 50., .x2 = 125., .y2 = 125.};

	AGlActor* split_lines = agl_actor__add_child((AGlActor*)scene, split_lines_actor(NULL));
	split_lines->region = (AGlfRegion){.x1 = 50., .y1 = 50., .x2 = 175., .y2 = 200.};

	g_main_loop_run(agl_main_loop_new());

	agl_window_destroy(&window);
	XCloseDisplay(dpy);

	return 0;
}


AGlActor*
lines_actor (void* view)
{
	static GLuint buffer_id;
	static AGlTQuad vertex[N_LINES] = {0,};

	agl = agl_get_instance();

	void lines_init (AGlActor* actor)
	{
		if (!buffer_id) glGenBuffers (1, &buffer_id);
		if (!agl->aaline) agl->aaline = agl_aa_line_new();

		for (int i=0;i<N_LINES;i++) {
			agl_aa_line_set_line((AGlTQuad(*)[])vertex, i,
				(AGlfPt){(i    ) * 10., 100. * sinf((i    ) * (M_PI_4 / 4.))},
				(AGlfPt){(i + 1) * 10., 100. * sinf((i + 1) * (M_PI_4 / 4.))}
			);
		}

		glBindBuffer (GL_ARRAY_BUFFER, buffer_id);
		glBufferData (GL_ARRAY_BUFFER, sizeof(vertex), vertex, GL_STATIC_DRAW);
	}

	void lines_free (AGlActor* actor)
	{
		glDeleteBuffers (1, &buffer_id);
	}

	void lines_set_state (AGlActor* actor)
	{
		agl->shaders.alphamap->uniform.fg_colour = ((AGlActor*)actor)->colour;
		agl_use_material(agl->aaline);
	}

	bool lines_paint (AGlActor* actor)
	{
		glBindBuffer (GL_ARRAY_BUFFER, buffer_id);
		glEnableVertexAttribArray (0);
		glVertexAttribPointer (0, 4, GL_FLOAT, GL_FALSE, 2 * sizeof(AGlVertex), NULL);

		glDrawArrays (GL_TRIANGLES, 0, G_N_ELEMENTS(vertex) * 6);

		return true;
	}

	static AGlActorClass lines_class = {0, "Lines"};
	lines_class.free = lines_free;

	return agl_actor__new(AGlActor,
		.class = &lines_class,
		.init = lines_init,
		.region = {
			.x2 = 1, .y2 = 1 // must have size else will not be rendered
		},
		.set_state = lines_set_state,
		.paint = lines_paint,
		.program = (AGlShader*)agl->shaders.alphamap,
		.colour = 0x33ff33ff
	);
}


/*
 *  This example uses two separate shader attributes for vertices and texture coordinates contained in a single VAO
 *
 *  It turns out not to be very useful because the same texture coordinates still need to be duplicated for each line.
 */
AGlActor*
split_lines_actor (void* view)
{
	static unsigned int vao = 0;
	static GLuint buffer_id[2];
	static AGlQuadVertex vertex[N_LINES] = {0,};
	static AGlQuadVertex tquad[N_LINES] = {0,};

	agl = agl_get_instance();

	void add_line (int i, AGlfPt p0, AGlfPt p1)
	{
		float len = sqrtf(powf((p1.y - p0.y), 2.) + powf((p1.x - p0.x), 2.));
		float xoff = (p1.y - p0.y) * 5. / len;
		float yoff = (p1.x - p0.x) * 5. / len;

		vertex[i] = (AGlQuadVertex){
			(AGlVertex){p0.x - xoff/2, p0.y + yoff/2},
			(AGlVertex){p1.x - xoff/2, p1.y + yoff/2},
			(AGlVertex){p1.x + xoff/2, p1.y - yoff/2},
			(AGlVertex){p0.x - xoff/2, p0.y + yoff/2},
			(AGlVertex){p1.x + xoff/2, p1.y - yoff/2},
			(AGlVertex){p0.x + xoff/2, p0.y - yoff/2},
		};

		tquad[i] = (AGlQuadVertex){
			(AGlVertex){0.0, 0.0},
			(AGlVertex){1.0, 0.0},
			(AGlVertex){1.0, 1.0},
			(AGlVertex){0.0, 0.0},
			(AGlVertex){1.0, 1.0},
			(AGlVertex){0.0, 1.0},
		};
	}

	void lines_init (AGlActor* actor)
	{
		if (!vao) glGenVertexArrays(1, &vao);

		if (!agl->aaline) agl->aaline = agl_aa_line_new();

		for (int i=0;i<N_LINES;i++) {
			add_line(i,
				(AGlfPt){(i    ) * 10., 100. * sinf((i    ) * (M_PI_4 / 4.))},
				(AGlfPt){(i + 1) * 10., 100. * sinf((i + 1) * (M_PI_4 / 4.))}
			);
		}

		agl_use_va (vao);

		glGenBuffers (2, buffer_id);

		glBindBuffer (GL_ARRAY_BUFFER, buffer_id[0]);
		glBufferData (GL_ARRAY_BUFFER, sizeof(vertex), vertex, GL_STATIC_DRAW);

		glEnableVertexAttribArray (0);
		glVertexAttribPointer (0, VEC2, GL_FLOAT, GL_FALSE, 0, NULL);

		glBindBuffer (GL_ARRAY_BUFFER, buffer_id[1]);
		glBufferData (GL_ARRAY_BUFFER, sizeof(tquad), &tquad, GL_STATIC_DRAW);

		glEnableVertexAttribArray (1);
		glVertexAttribPointer (1, VEC2, GL_FLOAT, GL_FALSE, 0, NULL);
	}

	void lines_free (AGlActor* actor)
	{
		glDeleteBuffers (2, buffer_id);
	}

	void lines_set_state (AGlActor* actor)
	{
		agl->shaders.alphamap_split->uniform.fg_colour = ((AGlActor*)actor)->colour;
		agl_enable(AGL_ENABLE_BLEND);
		agl_use_texture(aaline_class.texture);
	}

	bool lines_paint (AGlActor* actor)
	{
		agl_use_va (vao);
		agl_translate(&agl->shaders.alphamap_split->shader, 0, 0);
		glDrawArrays (GL_TRIANGLES, 0, G_N_ELEMENTS(vertex) * 6);

		return true;
	}

	static AGlActorClass split_lines_class = {0, "SplitLines"};
	split_lines_class.free = lines_free;

	return agl_actor__new(AGlActor,
		.class = &split_lines_class,
		.init = lines_init,
		.region = {
			.x2 = 1, .y2 = 1 // must have size else will not be rendered
		},
		.set_state = lines_set_state,
		.paint = lines_paint,
		.program = (AGlShader*)agl->shaders.alphamap_split,
		.colour = 0xff9933ff
	);
}
