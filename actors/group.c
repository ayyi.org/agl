/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://ayyi.org               |
 | copyright (C) 2012-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#undef USE_GTK
#include "agl/actor.h"
#include "group.h"

static AGlActorClass actor_class = {0, "Group", (AGlActorNew*)group_actor};


AGlActor*
group_actor (void* _)
{
	AGlActor* actor = agl_actor__new(AGlActor, .class = &actor_class);

	return actor;
}

