/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __common2_h__
#define __common2_h__

#include "agl/typedefs.h"
#include "debug/debug.h"

typedef void (KeyHandler) (gpointer);

typedef struct
{
	int         key;
	KeyHandler* handler;
} Key;

void add_key_handlers         (Key keys[]);

#endif
