/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2018-2024 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include <X11/Xlib.h>
#include "agl/actor.h"

extern Display* dpy;

typedef struct {
    Window    window;
    AGlScene* scene;
} AGlWindow;

typedef enum {
    AGL_FLAGS_NONE = 0,
    AGL_MAXIMISED,
    AGL_FULLSCREEN,
} AGlWindowFlags;

AGlWindow* agl_window            (const char* name, int x, int y, int width, int height, AGlWindowFlags);
void       agl_window_destroy    (AGlWindow**);

void       agl_window_set_icons  (Window, GList* pixbufs);
bool       agl_is_fullscreen     (Window);
void       agl_toggle_fullscreen (Window);
void       agl_is_maximised      (Window, int* h, int* v);
void       agl_toggle_maximised  (Window);
void       agl_send_key_event    (AGlWindow*, AGlActor*, char* key, int modifiers);

GMainLoop* agl_main_loop_new     ();
