/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2021-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

/*
 |  PartialCacheBehaviour supports the case where the content of an
 |  actor is sparse and does not fill the entire actor.
 |
 |  The cache is often bigger than the viewport and is positioned such
 |  that the current viewport is at the cache's centre
 |  (also taking into account the start and end of the content).
 |
 |  Currently this is only implemented for the x dimension.
 */

#include "config.h"
#undef USE_GTK
#include "debug/debug.h"
#include "agl/fbo.h"
#include "partial_cache.h"

static void partial_cache_init   (AGlBehaviour*, AGlActor*);
static void partial_cache_layout (AGlBehaviour*, AGlActor*);

static AGlBehaviourClass klass = {
	.new = partial_cache,
	.init = partial_cache_init,
	.layout = partial_cache_layout,
};


AGlBehaviourClass*
partial_cache_get_class ()
{
	return &klass;
}


AGlBehaviour*
partial_cache ()
{
	return (AGlBehaviour*)AGL_NEW(PartialCacheBehaviour,
		.behaviour = {
			.klass = &klass,
		}
	);
}


static void
partial_cache_init (AGlBehaviour* behaviour, AGlActor* actor)
{
}


static void
partial_cache_layout (AGlBehaviour* behaviour, AGlActor* actor)
{
	#define length(A) (A.end - A.start)

	PartialCacheBehaviour* pcb = (PartialCacheBehaviour*)behaviour;
	AGlActor* scrollable = pcb->scrollable;

	if (!scrollable) return;

	AGliRegion viewport = {
		.x1 = -scrollable->scrollable.x1,
		.x2 = -scrollable->scrollable.x1 + scrollable->region.x2,
		.y1 = -scrollable->scrollable.y1,
		.y2 = -scrollable->scrollable.y1 + scrollable->region.y2,
	};

	AGliRange content = pcb->content = pcb->get_range(actor);

	actor->cache.position.x = content.start;

	int w = length(content);
	if (content.start > viewport.x2 || content.end < viewport.x1) {
		actor->cache.size_request = (AGliPt){0,};
	} else if (w < AGL_MAX_FBO_WIDTH) {
		// all content fits in the cache
		actor->cache.size_request = (AGliPt){
			w,
			agl_actor__height(actor)
		};
	} else {
		int viewport_width = viewport.x2 - viewport.x1;
		g_return_if_fail(viewport_width <= AGL_MAX_FBO_WIDTH);

		if (content.start >= viewport.x1 || content.end <= viewport.x2) {

			actor->cache.size_request = (AGliPt){
				AGL_MAX_FBO_WIDTH,
				agl_actor__height(actor)
			};
		} else {
			// content extends outside the viewport to both the left and right
			float excess_left = viewport.x1 - content.start - 0.;
			float excess_right = content.end - viewport.x2 - 0.;
			float r = (float)(AGL_MAX_FBO_WIDTH - viewport_width) / (length(content) - viewport_width);
			int size_left = excess_left * r;
			int size_right = (float)excess_right * r;

			actor->cache.position.x = viewport.x1 - size_left;
			actor->cache.size_request = (AGliPt){
				viewport.x2 + size_right - actor->cache.position.x,
				agl_actor__height(actor)
			};
		}
	}
}
