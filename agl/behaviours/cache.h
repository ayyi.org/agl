/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2020-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include "glib.h"
#include "agl/actor.h"
#include "agl/observable.h"

#ifdef AGL_ACTOR_RENDER_CACHE

typedef struct {
   AGlBehaviour behaviour;
   GArray*      dependencies;
   AGlActorFn   on_invalidate;
} CacheBehaviour;

AGlBehaviourClass* cache_get_class ();

AGlBehaviour* cache_behaviour                ();
void          cache_behaviour_add_dependency (CacheBehaviour*, AGlActor*, AGlObservable*);

#endif
