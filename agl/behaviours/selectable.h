/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2016-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include "glib.h"
#include "agl/observable.h"
#include "agl/actor.h"
#include "agl/behaviour.h"

typedef struct {
   AGlBehaviour    behaviour;
   AGlObservable*  observable;
   AGlObservableFn on_select;
} SelectBehaviour;

AGlBehaviourClass* selectable_get_class ();

AGlBehaviour* selectable      ();
