/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2019-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include "config.h"
#undef USE_GTK
#include "fullsize.h"

static void fullsize_layout (AGlBehaviour*, AGlActor*);

static AGlBehaviourClass klass = {
	.new = fullsize,
	.layout = fullsize_layout
};


AGlBehaviourClass*
fullsize_get_class ()
{
	return &klass;
}


AGlBehaviour*
fullsize ()
{
	FullsizeBehaviour* a = AGL_NEW(FullsizeBehaviour,
		.behaviour = {
			.klass = &klass,
		},
	);

	return (AGlBehaviour*)a;
}


static void
fullsize_layout (AGlBehaviour* behaviour, AGlActor* actor)
{
	if (actor->parent) actor->region = actor->parent->region;
}

