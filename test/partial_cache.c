/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2021-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#define __no_setup__

#include "config.h"
#include <glib.h>
#include <stdio.h>
#include "debug/debug.h"
#include "agl/fbo.h"
#include "agl/behaviours/partial_cache.h"
#include "test/runner.h"


void
test_partial_cache_behaviour ()
{
	START_TEST;

	static AGliRange content = {0, 100};

	typedef struct {
		int x;
		int len;
	} Expect;

	AGliRange get_content_range (AGlActor* actor)
	{
		return content;
	}
	AGlBehaviour* cache = partial_cache ();
	PartialCacheBehaviour* pcb; pcb = (PartialCacheBehaviour*)cache;

	AGlActor actor = {
		.region = { .x2 = 100, .y2 = 100, }
	};
	AGlActor scrollable = {
		.scrollable = {
			.x1 = - 100,
			.x2 = AGL_MAX_FBO_WIDTH + 1000,
		},
		.region = { .x2 = 100 }
	};
	pcb->scrollable = &scrollable;
	pcb->get_range = get_content_range;

	AGlBehaviourClass* klass = partial_cache_get_class ();

	int i = 0;

	#define try() \
		i++; \
		klass->layout ((AGlBehaviour*)cache, &actor); \
		assert(actor.cache.position.x == expect.x, "%i: x: expected %i got %i", i, expect.x, actor.cache.position.x); \
		assert(actor.cache.size_request.x == expect.len, "%i: size: expected %i got %i", i, expect.len, actor.cache.size_request.x);

	content = (AGliRange){0, 100};
	Expect expect = {0, 100};
	try();

	// content is fully in viewport, 
	content = (AGliRange){110, 120};
	expect = (Expect){110, 10};
	try();

	// content is not in viewport (to left) 
	content = (AGliRange){10, 20};
	expect = (Expect){10, 0};
	try();

	// content is not in viewport (to right)
	content = (AGliRange){210, 220};
	expect = (Expect){210, 0};
	try();

	// content is partially in viewport
	content = (AGliRange){100, 300};
	expect = (Expect){100, 200};
	try();
	content = (AGliRange){150, 300};
	expect = (Expect){150, 150};
	try();

	// content exceeds max cache size and extends only to the right
	content = (AGliRange){100, AGL_MAX_FBO_WIDTH + 200};
	expect = (Expect){100, AGL_MAX_FBO_WIDTH};
	try();

	// content exceeds max cache size and extends only to the left
	scrollable.scrollable.x1 = -AGL_MAX_FBO_WIDTH;
	content = (AGliRange){50, AGL_MAX_FBO_WIDTH + 100};
	expect = (Expect){50, AGL_MAX_FBO_WIDTH};
	try();

	// content exceeds max cache size and extends equally to the left and to the right
	content = (AGliRange){AGL_MAX_FBO_WIDTH / 2, AGL_MAX_FBO_WIDTH + 100 + AGL_MAX_FBO_WIDTH / 2};
	expect = (Expect){2098, AGL_MAX_FBO_WIDTH};
	try();

	// content exceeds max cache size and extends more to the left
	content.start -= 100;
	content.end -= 100;
	expect = (Expect){2001, AGL_MAX_FBO_WIDTH - 1};
	try();

	content = (AGliRange){AGL_MAX_FBO_WIDTH / 4, AGL_MAX_FBO_WIDTH + 100 + AGL_MAX_FBO_WIDTH / 4};
	expect = (Expect){1099, AGL_MAX_FBO_WIDTH};
	try();

	content = (AGliRange){50, AGL_MAX_FBO_WIDTH + 100 + 50};
	expect = (Expect){149, AGL_MAX_FBO_WIDTH - 1};
	try();

	// content takes up most of the space
	content = (AGliRange){50, AGL_MAX_FBO_WIDTH + 100 + AGL_MAX_FBO_WIDTH - 50};
	expect = (Expect){2098, AGL_MAX_FBO_WIDTH};
	try();

	g_free(cache);

	FINISH_TEST;
}

gpointer tests[] = {
	test_partial_cache_behaviour,
};

#include "test/setup.c"
