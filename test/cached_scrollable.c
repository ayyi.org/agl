/*
 +---------------------------------------------------------------------
 |
 | This file is part of the Ayyi project. http://www.ayyi.org          
 | copyright (C) 2012-2021 Tim Orford <tim@orford.org>                 
 |
 +---------------------------------------------------------------------
 |
 | This program is free software; you can redistribute it and/or modify
 | it under the terms of the GNU General Public License version 3      
 | as published by the Free Software Foundation.                       
 |
 +---------------------------------------------------------------------
 |
 | test/cached_scrollable.c
 |
 | Test the use-case where the complete contents of an actor are cached
 | and are scrollable within a smaller viewport.
 |
 */

#define __glx_test__
#include "config.h"
#undef USE_GTK
#include <getopt.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include "gdk/gdk.h"
#include "agl/ext.h"
#include "agl/x11.h"
#include "agl/actor.h"
#include "test/common2.h"
#include "agl/shader.h"
#include "agl/behaviours/border.h"
#include "agl/behaviours/scrollable.h"

extern char* basename (const char*);

AGlActor* cached_actor (void*);

#define SCROLLABLE_HEIGHT 308.
#define agl_actor__scrollable_height(A) (A->scrollable.y2 - A->scrollable.y1)

static KeyHandler
	nav_up,
	nav_down,
	zoom_in,
	zoom_out;

Key keys[] = {
	{XK_Up,          nav_up},
	{XK_Down,        nav_down},
	{XK_equal,       zoom_in},
	{XK_KP_Add,      zoom_in},
	{XK_minus,       zoom_out},
	{XK_KP_Subtract, zoom_out},
};

static const struct option long_options[] = {
	{ "help",             0, NULL, 'h' },
	{ "non-interactive",  0, NULL, 'n' },
};

static const char* const short_options = "nh";

static const char* const usage =
	"Usage: %s [OPTIONS]\n\n"
	"\n"
	"Options:\n"
	"  --help\n"
	"  --non-interative\n"
	"\n";


int
main (int argc, char* argv[])
{
	int width = 300, height = 300;

	if (g_getenv("NON_INTERACTIVE")) {
		g_timeout_add(3000, (gpointer)exit, NULL);
	}

	int opt;
	while ((opt = getopt_long (argc, argv, short_options, long_options, NULL)) != -1) {
		switch (opt) {
			case 'h':
				printf (usage, basename(argv[0]));
				exit(EXIT_SUCCESS);
				break;
			case 'n':
				g_timeout_add(3000, (gpointer)exit, NULL);
				break;
		}
	}

	AGlWindow* window = agl_window ("cachedscrollable", 0, 0, width, height, 0);

	AGlActor* a = agl_actor__add_child ((AGlActor*)window->scene, cached_actor(NULL));
	a->region = (AGlfRegion){120., 10., 220., 210.};
	a->scrollable = (AGliRegion){0., 0., 100., SCROLLABLE_HEIGHT};
	a->behaviours[1] = border();

	add_key_handlers (keys);

	g_main_loop_run (agl_main_loop_new());

	agl_window_destroy (&window);
	XCloseDisplay (dpy);

	return 0;
}


static void
nav_up (gpointer user_data)
{
}


static void
nav_down (gpointer user_data)
{
}


static void
zoom_in (gpointer user_data)
{
}


static void
zoom_out (gpointer user_data)
{
}


AGlActor*
cached_actor (void* view)
{
	void cached_init (AGlActor* actor)
	{
		agl_set_font ("sans", 10, PANGO_WEIGHT_NORMAL);
		agl_actor__set_size (actor);
	}

	void texture_set_state (AGlActor* actor)
	{
		PLAIN_COLOUR2 (actor->program) = 0x999999ff;
	}

	void cached_set_size (AGlActor* actor)
	{
	}

	bool cached_paint (AGlActor* actor)
	{
		/*
		 *  Note painting here is offset by actor->scrollable which would not normally be the case
		 */
		for (int i=0; i < 19; i++) {
			agl_print (10, -actor->scrollable.y1 + i * 16, 0, 0xffffffff, "%c%c", 65 + i, 65 + i);
		}

		return true;
	}

	void cached_free ()
	{
	}

	static AGlActorClass cached_class = {0, "Cached2"};
	cached_class.free = cached_free;
	agl_actor_class__add_behaviour (&cached_class, scrollable_get_class());

	return agl_actor__new (AGlActor,
		.class = &cached_class,
		.name = cached_class.name,
		.init = cached_init,
		.set_size = cached_set_size,
		.set_state = texture_set_state,
		.paint = cached_paint,
		.program = agl_get_instance()->shaders.plain,
	);
}
