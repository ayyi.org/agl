/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 */

#ifndef __scrollbar_h__
#define __scrollbar_h__

#include "agl/actor.h"
#include "agl/observable.h"

AGlActorClass* scrollbar_view_get_class ();

AGlActor* scrollbar_view (AGlActor*, AGlOrientation, AGlObservable*, AGlObservable*, int step_size);

#endif
