/**
 * Utility for getting OpenGL extension function pointers
 */
#ifndef __agl_ext_h__
#define __agl_ext_h__

#ifdef USE_EPOXY
# include <epoxy/gl.h>
# include <epoxy/glx.h>
#else
# include <GL/glx.h>

void agl_get_extensions ();

#undef EXTERN
#ifdef __agl_ext_c__
#define EXTERN
#else
#define EXTERN extern
#endif

#ifndef APIENTRYP
	#define APIENTRYP APIENTRY *
#endif

EXTERN PFNGLBLENDFUNCSEPARATEPROC glBlendFuncSeparate;

/* OpenGL 2.0 */
EXTERN PFNGLATTACHSHADERPROC glAttachShader;
EXTERN PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation;
EXTERN PFNGLCOMPILESHADERPROC glCompileShader;
EXTERN PFNGLCREATEPROGRAMPROC glCreateProgram;
EXTERN PFNGLCREATESHADERPROC glCreateShader;
EXTERN PFNGLDELETEPROGRAMPROC glDeleteProgram;
EXTERN PFNGLDELETESHADERPROC glDeleteShader;
EXTERN PFNGLDETACHSHADERPROC glDetachShader;
#if 0
PFNGLGETACTIVEATTRIBPROC glGetActiveAttrib_func;
PFNGLGETACTIVEUNIFORMPROC glGetActiveUniform_func;
PFNGLGETATTACHEDSHADERSPROC glGetAttachedShaders_func;
PFNGLGETATTRIBLOCATIONPROC glGetAttribLocation_func;
#endif
EXTERN PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;
EXTERN PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog;
EXTERN PFNGLGETSHADERIVPROC glGetShaderiv;
EXTERN PFNGLGETPROGRAMIVPROC glGetProgramiv;
EXTERN PFNGLGETSHADERSOURCEPROC glGetShaderSource;
EXTERN PFNGLGETUNIFORMFVPROC glGetUniformfv;
EXTERN PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;
EXTERN PFNGLISPROGRAMPROC glIsProgram;
EXTERN PFNGLISSHADERPROC glIsShader;
EXTERN PFNGLLINKPROGRAMPROC glLinkProgram;
EXTERN PFNGLSHADERSOURCEPROC glShaderSource;
EXTERN PFNGLUNIFORM1IPROC glUniform1i;
EXTERN PFNGLUNIFORM2IPROC glUniform2i_func;
EXTERN PFNGLUNIFORM3IPROC glUniform3i_func;
EXTERN PFNGLUNIFORM4IPROC glUniform4i_func;
EXTERN PFNGLUNIFORM1FPROC glUniform1f;
EXTERN PFNGLUNIFORM2FPROC glUniform2f;
EXTERN PFNGLUNIFORM3FPROC glUniform3f_func;
EXTERN PFNGLUNIFORM4FPROC glUniform4f;
EXTERN PFNGLUNIFORM1FVPROC glUniform1fv;
EXTERN PFNGLUNIFORM2FVPROC glUniform2fv;
EXTERN PFNGLUNIFORM3FVPROC glUniform3fv;
EXTERN PFNGLUNIFORM4FVPROC glUniform4fv;
EXTERN PFNGLUNIFORM1IVPROC glUniform1iv;
#if 0
static PFNGLUNIFORMMATRIX2FVPROC glUniformMatrix2fv_func = NULL;
static PFNGLUNIFORMMATRIX3FVPROC glUniformMatrix3fv_func = NULL;
#endif
EXTERN PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv;

typedef void (APIENTRYP PFNGLUSEPROGRAMPROC) (GLuint program);
EXTERN PFNGLUSEPROGRAMPROC glUseProgram;
#if 0
static PFNGLVERTEXATTRIB1FPROC glVertexAttrib1f_func = NULL;
static PFNGLVERTEXATTRIB2FPROC glVertexAttrib2f_func = NULL;
static PFNGLVERTEXATTRIB3FPROC glVertexAttrib3f_func = NULL;
static PFNGLVERTEXATTRIB4FPROC glVertexAttrib4f_func = NULL;
static PFNGLVERTEXATTRIB1FVPROC glVertexAttrib1fv_func = NULL;
static PFNGLVERTEXATTRIB2FVPROC glVertexAttrib2fv_func = NULL;
static PFNGLVERTEXATTRIB3FVPROC glVertexAttrib3fv_func = NULL;
static PFNGLVERTEXATTRIB4FVPROC glVertexAttrib4fv_func = NULL;
#endif
EXTERN PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer;
EXTERN PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
#if 0
static PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray_func = NULL;

/* OpenGL 2.1 */
static PFNGLUNIFORMMATRIX2X3FVPROC glUniformMatrix2x3fv_func = NULL;
static PFNGLUNIFORMMATRIX3X2FVPROC glUniformMatrix3x2fv_func = NULL;
static PFNGLUNIFORMMATRIX2X4FVPROC glUniformMatrix2x4fv_func = NULL;
static PFNGLUNIFORMMATRIX4X2FVPROC glUniformMatrix4x2fv_func = NULL;
static PFNGLUNIFORMMATRIX3X4FVPROC glUniformMatrix3x4fv_func = NULL;
static PFNGLUNIFORMMATRIX4X3FVPROC glUniformMatrix4x3fv_func = NULL;

/* OpenGL 1.4 */
static PFNGLPOINTPARAMETERFVPROC glPointParameterfv_func = NULL;
static PFNGLSECONDARYCOLOR3FVPROC glSecondaryColor3fv_func = NULL;

/* GL_ARB_vertex/fragment_program */
static PFNGLBINDPROGRAMARBPROC glBindProgramARB_func = NULL;
static PFNGLDELETEPROGRAMSARBPROC glDeleteProgramsARB_func = NULL;
static PFNGLGENPROGRAMSARBPROC glGenProgramsARB_func = NULL;
static PFNGLGETPROGRAMLOCALPARAMETERDVARBPROC glGetProgramLocalParameterdvARB_func = NULL;
static PFNGLISPROGRAMARBPROC glIsProgramARB_func = NULL;
static PFNGLPROGRAMLOCALPARAMETER4DARBPROC glProgramLocalParameter4dARB_func = NULL;
static PFNGLPROGRAMLOCALPARAMETER4FVARBPROC glProgramLocalParameter4fvARB_func = NULL;
static PFNGLPROGRAMSTRINGARBPROC glProgramStringARB_func = NULL;
static PFNGLVERTEXATTRIB1FARBPROC glVertexAttrib1fARB_func = NULL;

/* GL_APPLE_vertex_array_object */
#endif
EXTERN PFNGLBINDVERTEXARRAYPROC glBindVertexArray;
EXTERN PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays;
EXTERN PFNGLGENVERTEXARRAYSPROC glGenVertexArrays;
#if 0
static PFNGLISVERTEXARRAYAPPLEPROC glIsVertexArrayAPPLE_func = NULL;

/* GL_EXT_stencil_two_side */
static PFNGLACTIVESTENCILFACEEXTPROC glActiveStencilFaceEXT_func = NULL;

/* GL_ARB_buffer_object */
#endif
EXTERN PFNGLGENBUFFERSPROC glGenBuffers;
EXTERN PFNGLDELETEBUFFERSPROC glDeleteBuffers;
EXTERN PFNGLBINDBUFFERPROC glBindBuffer;
EXTERN PFNGLBUFFERDATAPROC glBufferData;
#if 0
static PFNGLBUFFERSUBDATAARBPROC glBufferSubDataARB_func = NULL;
static PFNGLMAPBUFFERARBPROC glMapBufferARB_func = NULL;
static PFNGLUNMAPBUFFERARBPROC glUnmapBufferARB_func = NULL;

/* GL_EXT_framebuffer_object */
static PFNGLISRENDERBUFFEREXTPROC glIsRenderbufferEXT_func = NULL;
static PFNGLBINDRENDERBUFFEREXTPROC glBindRenderbufferEXT_func = NULL;
static PFNGLDELETERENDERBUFFERSEXTPROC glDeleteRenderbuffersEXT_func = NULL;
static PFNGLGENRENDERBUFFERSEXTPROC glGenRenderbuffersEXT_func = NULL;
static PFNGLRENDERBUFFERSTORAGEEXTPROC glRenderbufferStorageEXT_func = NULL;
static PFNGLGETRENDERBUFFERPARAMETERIVEXTPROC glGetRenderbufferParameterivEXT_func = NULL;
static PFNGLISFRAMEBUFFEREXTPROC glIsFramebufferEXT_func = NULL;
static PFNGLDELETEFRAMEBUFFERSEXTPROC glDeleteFramebuffersEXT_func = NULL;
static PFNGLGENFRAMEBUFFERSEXTPROC glGenFramebuffersEXT = NULL;
static PFNGLCHECKFRAMEBUFFERSTATUSEXTPROC glCheckFramebufferStatusEXT = NULL;
static PFNGLFRAMEBUFFERTEXTURE1DEXTPROC glFramebufferTexture1DEXT_func = NULL;
static PFNGLFRAMEBUFFERTEXTURE2DEXTPROC glFramebufferTexture2DEXT_func = NULL;
static PFNGLFRAMEBUFFERTEXTURE3DEXTPROC glFramebufferTexture3DEXT_func = NULL;
static PFNGLFRAMEBUFFERRENDERBUFFEREXTPROC glFramebufferRenderbufferEXT_func = NULL;
static PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVEXTPROC glGetFramebufferAttachmentParameterivEXT_func;
static PFNGLGENERATEMIPMAPEXTPROC glGenerateMipmapEXT_func;

/* GL_ARB_framebuffer_object */
static PFNGLISRENDERBUFFERPROC glIsRenderbuffer_func;
#endif
EXTERN PFNGLBINDRENDERBUFFERPROC glBindRenderbuffer;
EXTERN PFNGLDELETERENDERBUFFERSPROC glDeleteRenderbuffers;
EXTERN PFNGLGENRENDERBUFFERSPROC glGenRenderbuffers;
EXTERN PFNGLRENDERBUFFERSTORAGEPROC glRenderbufferStorage;
EXTERN PFNGLGETRENDERBUFFERPARAMETERIVPROC glGetRenderbufferParameteriv;
#if 0
static PFNGLISFRAMEBUFFERPROC glIsFramebuffer_func;
#endif
EXTERN PFNGLBINDFRAMEBUFFERPROC glBindFramebuffer;
EXTERN PFNGLDELETEFRAMEBUFFERSPROC glDeleteFramebuffers;
EXTERN PFNGLGENFRAMEBUFFERSPROC glGenFramebuffers;
EXTERN PFNGLCHECKFRAMEBUFFERSTATUSPROC glCheckFramebufferStatus;
EXTERN PFNGLFRAMEBUFFERTEXTURE1DPROC glFramebufferTexture1D;
EXTERN PFNGLFRAMEBUFFERTEXTURE2DPROC glFramebufferTexture2D;
#if 0
static PFNGLFRAMEBUFFERTEXTURE3DPROC glFramebufferTexture3D_func;
#endif
EXTERN PFNGLFRAMEBUFFERRENDERBUFFERPROC glFramebufferRenderbuffer;
EXTERN PFNGLGETFRAMEBUFFERATTACHMENTPARAMETERIVPROC glGetFramebufferAttachmentParameteriv;
#if 0
static PFNGLGENERATEMIPMAPPROC glGenerateMipmap_func;
static PFNGLBLITFRAMEBUFFERPROC glBlitFramebuffer_func;
static PFNGLRENDERBUFFERSTORAGEMULTISAMPLEPROC glRenderbufferStorageMultisample_func;
static PFNGLFRAMEBUFFERTEXTURELAYERPROC glFramebufferTextureLayer_func;
#endif
EXTERN PFNGLSTRINGMARKERGREMEDYPROC glStringMarkerGREMEDY;

EXTERN PFNGLBEGINQUERYPROC glBeginQuery;
EXTERN PFNGLENDQUERYPROC glEndQuery;

/* GL_VERSION_3_3 */
#if 0
PFNGLGETQUERYOBJECTIVPROC glGetQueryObjectiv;
PFNGLGETQUERYOBJECTUI64VPROC glGetQueryObjectui64v;
#endif

/* GL_VERSION_4_3 */
#if 0
EXTERN PFNGLINVALIDATEBUFFERDATAPROC glInvalidateBufferData;
#endif

#undef EXTERN

#endif // USE_EPOXY
#endif
