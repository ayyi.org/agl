/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2016-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __agl_clip_h__
#define __agl_clip_h__

#include "glib.h"
#include "agl/actor.h"

typedef struct {
   AGlBehaviour   behaviour;
} ClipBehaviour;

AGlBehaviourClass* clip_get_class ();

AGlBehaviour* clip ();

#endif
