/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2012-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#undef USE_GTK
#include <sys/time.h>
#include "agl/actor.h"
#include "agl/shader.h"
#include "actors/plain.h"

static AGl* agl = NULL;


static void
plain_set_state (AGlActor* actor)
{
	if (agl->use_shaders) {
		SET_PLAIN_COLOUR(actor->program, actor->colour);
	} else {
		glColor4f(0.4, 1.0, 0.4, 1.0);
	}
}


static bool
plain_paint (AGlActor* actor)
{
	agl_rect(
		0,
		0,
		agl_actor__width(actor),
		agl_actor__height(actor)
	);

	return true;
}


AGlActor*
plain_actor (void* _)
{
	agl = agl_get_instance();

	return agl_actor__new (AGlActor,
		.name = g_strdup("plain"),
		.region = {
			.x2 = 1, .y2 = 1 // must have size else will not be rendered
		},
		.set_state = plain_set_state,
		.paint = plain_paint,
		.program = agl->shaders.plain,
	);
}
