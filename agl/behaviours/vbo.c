/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2021-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#undef USE_GTK
#include "agl/debug.h"
#include "agl/text/renderer.h"
#include "agl/text/roundedrect.h"
#include "vbo.h"

static void vbo_behaviour_free (AGlBehaviour*);
static void vbo_init (AGlBehaviour*, AGlActor*);

static AGlBehaviourClass klass = {
	.new = vbo_behaviour,
	.free = vbo_behaviour_free,
	.init = vbo_init,
};


AGlBehaviourClass*
vbo_behaviour_get_class ()
{
	return &klass;
}


AGlBehaviour*
vbo_behaviour ()
{
	VboBehaviour* a = AGL_NEW(VboBehaviour,
		.behaviour = {
			.klass = &klass,
		}
	);

	return (AGlBehaviour*)a;
}


static void
vbo_behaviour_free (AGlBehaviour* behaviour)
{
	VboBehaviour* v = (VboBehaviour*)behaviour;

	glDeleteBuffers(1, &v->vbo);
	g_clear_pointer(&v->data, g_free);
	g_free(behaviour);
}


static void
vbo_init (AGlBehaviour* behaviour, AGlActor* actor)
{
	VboBehaviour* v = (VboBehaviour*)behaviour;

	if (!v->vbo) {
		glGenBuffers(1, &v->vbo);
	}
}

