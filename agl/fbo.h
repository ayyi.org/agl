/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2013-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include "agl/typedefs.h"

typedef enum {
	AGL_FBO_HAS_STENCIL = 1,
} AGlFBOFlags;

struct _AGlFBO {
	uint32_t    id;
	uint32_t    texture;
	int         width;      // the size that is being used. the texture may be bigger.
	int         height;
	AGlFBOFlags flags;
};

AGlFBO* agl_fbo_new      (int width, int height, uint32_t texture, AGlFBOFlags);
void    agl_fbo_free     (AGlFBO*);
void    agl_fbo_set_size (AGlFBO*, int width, int height);

#define agl_fbo_free0(var) (var = (agl_fbo_free(var), NULL))

typedef struct {
   AGlFBO* fb[10];
   int      i;
} FbStack;

#ifdef __agl_fbo_c__
FbStack fbs = {{0,},};
#else
extern FbStack fbs;
#endif

#define agl_draw_to_fbo(F) \
	fbs.fb[++fbs.i] = F; \
	glBindFramebuffer(GL_FRAMEBUFFER_EXT, F->id); \
	glViewport(0, 0, F->width, F->height);

#define agl_end_draw_to_fbo \
	glBindFramebuffer(GL_FRAMEBUFFER_EXT, fbs.fb[--fbs.i]->id); \
	glViewport(0, 0, fbs.fb[fbs.i]->width, fbs.fb[fbs.i]->height);

#define AGL_MAX_FBO_WIDTH 4096
