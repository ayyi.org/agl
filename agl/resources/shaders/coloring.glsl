// VERTEX_SHADER:
uniform vec4 u_color;

varying vec4 final_color;

void main() {
	gl_Position = vec4(
		(aPosition.x + translate.x) / modelview.x - 1.,
		-(aPosition.y + translate.y) / modelview.y + 1.,
		0.0,
		1.0
	);

	vUv = vec2(aUv.x, aUv.y);

	final_color = u_color;
	// pre-multiply
	final_color.rgb *= final_color.a;
	final_color *= u_alpha;
}

// FRAGMENT_SHADER:

_IN_ vec4 final_color;

void main() {
	vec4 diffuse = Texture(u_source, vUv);

	// u_clip_rect is a rect (x,y,w,h) followed by 2 corner sizes followed by 2 more corner sizes

	//setOutputColor(final_color * diffuse.a);

	// TODO fix projection so y origin is not the bottom of window
	float x = gl_FragCoord.x;
	float y = modelview.y * 2. - gl_FragCoord.y;

	// TODO if we are always clipping, what is the point of the viewport?
	// -fragcoord is relative to viewport?
	if(
		x > u_clip_rect[0][0] && x < u_clip_rect[0][0] + u_clip_rect[0][2] &&
		y > u_clip_rect[0][1] && y < u_clip_rect[0][1] + u_clip_rect[0][3]
	)
		gl_FragColor = final_color * diffuse.a;
	else
		gl_FragColor = vec4(0.0);
}
