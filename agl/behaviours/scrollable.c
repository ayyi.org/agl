/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2021-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 | behaviours/scrollable.c
 |
 | Cached scrollable area for use where the contents do not change often.
 | The render cache is preserved through scroll operations.
 | There is a constraint on the max size of the area in that it must
 | fit in a single FBO.
 |
 */

#include "config.h"
#include "debug/debug.h"
#undef USE_GTK
#include "agl/fbo.h"
#include "actors/scrollbar.h"
#include "scrollable.h"

#define SCROLL_INCREMENT 10
#define agl_actor__scrollable_height(A) ((A)->scrollable.y2 - (A)->scrollable.y1)
#define max_scroll_offset (agl_actor__scrollable_height(actor) - (int)agl_actor__height(actor))

static void scrollable_free      (AGlBehaviour*);
static void scrollable_init      (AGlBehaviour*, AGlActor*);
static void scrollable_layout    (AGlBehaviour*, AGlActor*);
static bool scrollable_event     (AGlBehaviour*, AGlActor*, GdkEvent*);
static void scrollable_on_scroll (AGlObservable*, AGlVal, gpointer);

static AGlBehaviourClass klass = {
	.new = scrollable,
	.free = scrollable_free,
	.init = scrollable_init,
	.layout = scrollable_layout,
	.event = scrollable_event
};


AGlBehaviourClass*
scrollable_get_class ()
{
	return &klass;
}


AGlBehaviour*
scrollable ()
{
	ScrollableBehaviour* a = AGL_NEW (ScrollableBehaviour,
		.behaviour = {
			.klass = &klass,
		},
		.scroll = agl_observable_new ()
	);

	return (AGlBehaviour*)a;
}


static void
scrollable_free (AGlBehaviour* behaviour)
{
	ScrollableBehaviour* scrollable = (ScrollableBehaviour*)behaviour;

	agl_observable_free(scrollable->scroll);

	g_free(scrollable);
}


void
scrollable_init (AGlBehaviour* behaviour, AGlActor* actor)
{
	ScrollableBehaviour* scrollable = (ScrollableBehaviour*)behaviour;

	agl_actor__add_child (actor->parent, scrollbar_view (actor, AGL_ORIENTATION_VERTICAL, scrollable->scroll, NULL, 1));
	agl_observable_subscribe(scrollable->scroll, scrollable_on_scroll, actor);
}


static AGlActor*
scrollable_find_scrollbar (AGlActor* actor)
{
	AGlActor* parent = actor->parent;
	if (!parent) return NULL;

	GList* l = parent->children;
	for (;l;l=l->next) {
		if (l->data == actor) {
			return l->next ? l->next->data : NULL;
		}
	}

	return NULL;
}


static void
scrollable_layout (AGlBehaviour* behaviour, AGlActor* actor)
{
	ScrollableBehaviour* scrollable = (ScrollableBehaviour*)behaviour;

	AGlActor* scrollbar = scrollable_find_scrollbar (actor);
	if (scrollbar) {
		scrollbar->region = (AGlfRegion){0., 0., agl_actor__width(actor), agl_actor__height(actor)};
		agl_actor__set_size (scrollbar);
	}

	scrollable->scroll->max.i = max_scroll_offset;

	if (actor->fbo) {
#ifdef AGL_ACTOR_RENDER_CACHE
		actor->cache.size_request.x = agl_actor__width(actor);
		agl_actor__invalidate(actor);
#endif
	} else {
		int height = agl_actor__scrollable_height (actor);
		if (height) {
#ifdef AGL_ACTOR_RENDER_CACHE
			actor->fbo = agl_fbo_new (agl_actor__width(actor), height, 0, AGL_FBO_HAS_STENCIL);
			actor->cache.enabled = true;
			actor->cache.size_request = (AGliPt){agl_actor__width(actor), height};
		}
#endif
	}
}


static bool
scrollable_event (AGlBehaviour* behaviour, AGlActor* actor, GdkEvent* event)
{
	ScrollableBehaviour* scrollable = (ScrollableBehaviour*)behaviour;

	bool scrollable_set_position (ScrollableBehaviour* scrollable, int scroll_offset)
	{

		g_return_val_if_fail (max_scroll_offset > -1, AGL_HANDLED);

		agl_observable_set_int (scrollable->scroll, (scroll_offset / SCROLL_INCREMENT) * SCROLL_INCREMENT);

		agl_scene_queue_draw ((AGlScene*)actor->root);

		return AGL_HANDLED;
	}

	switch(event->type){
		case GDK_BUTTON_PRESS:
			switch (event->button.button) {
				case 4:
					scrollable_set_position (scrollable, scrollable->scroll->value.i - SCROLL_INCREMENT);
					return AGL_HANDLED;
				case 5:
					if (agl_actor__scrollable_height (actor) > agl_actor__height(actor)) {
						return scrollable_set_position (scrollable, scrollable->scroll->value.i + SCROLL_INCREMENT);
					}
			}
			break;
		case GDK_SCROLL:
			scrollable_set_position (scrollable, ((GdkEventMotion*)event)->y);
			return AGL_HANDLED;
		default:
			break;
	}
	return AGL_NOT_HANDLED;
}


static void
scrollable_on_scroll (AGlObservable* o, AGlVal value, gpointer _actor)
{
	AGlActor* actor = _actor;

	int scrollable_height = agl_actor__scrollable_height(actor);
#ifdef AGL_ACTOR_RENDER_CACHE
	actor->cache.offset.y = - value.i;
	actor->scrollable.y1 = actor->cache.offset.y;
#else
	actor->scrollable.y1 = - value.i;
#endif
	actor->scrollable.y2 = actor->scrollable.y1 + scrollable_height;
}
