" Vim syntax file
" Language:     C agl

" typedefs.h
syn keyword aglType AGlShader
syn keyword aglType AGliPt
syn keyword aglType AGlfPt
syn keyword aglType AGlVertex
syn keyword aglType AGlQuadVertex
syn keyword aglType AGlTVertex
syn keyword aglType AGlTQuad
syn keyword aglType AGliSize
syn keyword aglType AGliRegion
syn keyword aglType AGlfRegion
syn keyword aglType AGlRect
syn keyword aglType AGlQuad
syn keyword aglType AGliRange
syn keyword aglType AGlColourFloat
syn keyword aglConstant AGL_V_PER_QUAD

" actor.h
syn keyword aglType        AGlActor
syn keyword aglType        AGlActorClass
syn keyword aglType        AGlActorType
syn keyword aglType        AGlScene
syn keyword aglType        AGlActorFn
syn keyword aglFunction    agl_actor__new
syn keyword aglFunction    agl_actor__width
syn keyword aglFunction    agl_actor__height
syn keyword aglFunction    agl_actor__new_root
syn keyword aglFunction    agl_actor__new_root_
syn keyword aglFunction    agl_actor__free
syn keyword aglFunction    agl_actor__add_child
syn keyword aglFunction    agl_actor__insert_child
syn keyword aglFunction    agl_actor__remove_child
syn keyword aglFunction    agl_actor__replace_child
syn keyword aglFunction    agl_actor__paint
syn keyword aglFunction    agl_actor__set_size
syn keyword aglFunction    agl_actor__scroll_to
syn keyword aglFunction    agl_actor__grab
syn keyword aglFunction    agl_actor__invalidate
syn keyword aglFunction    agl_actor__invalidate_down
syn keyword aglFunction    agl_actor__enable_cache
syn keyword aglFunction    agl_actor__start_transition
syn keyword aglFunction    agl_actor__is_disabled
syn keyword aglFunction    agl_actor__find_by_name
syn keyword aglFunction    agl_actor__find_by_class
syn keyword aglFunction    agl_actor__find_by_z
syn keyword aglFunction    agl_actor__pick
syn keyword aglFunction    agl_actor__pick_child
syn keyword aglFunction    agl_actor__find_offset
syn keyword aglFunction    agl_actor__on_expose
syn keyword aglFunction    agl_actor__find_behaviour
syn keyword aglFunction    agl_actor__null_painter
syn keyword aglFunction    agl_actor__solid_painter
syn keyword aglFunction    agl_actor__set_use_shaders
syn keyword aglFunction    agl_actor__on_event
syn keyword aglFunction    agl_actor__xevent
syn keyword aglFunction    agl_scene_queue_draw
syn keyword aglFunction    agl_scene_get_class
syn keyword aglFunction    agl_actor__add_behaviour
syn keyword aglFunction    agl_actor_class__add_behaviour
syn keyword aglFunction    agl_actor__get_behaviour
syn keyword aglFunction    agl_actor__print_tree

" utils.h
syn keyword aglType        AGlShaders
syn keyword aglType        AGlUniformLocation
syn keyword aglFunction    agl_get_instance
syn keyword aglFunction    agl_get_gl_context
syn keyword aglFunction    agl_enable
syn keyword aglFunction    agl_gl_init
syn keyword aglFunction    agl_free
syn keyword aglFunction    agl_shaders_supported
syn keyword aglFunction    agl_create_program
syn keyword aglFunction    agl_compile_shader_text
syn keyword aglFunction    agl_compile_shader_file
syn keyword aglFunction    agl_uniforms_init
syn keyword aglFunction    agl_link_shaders
syn keyword aglFunction    agl_use_program
syn keyword aglFunction    agl_use_program_id
syn keyword aglFunction    agl_use_texture
syn keyword aglFunction    agl_texture_unit_new
syn keyword aglFunction    agl_texture_unit_use_texture
syn keyword aglFunction    agl_colour_rbga
syn keyword aglFunction    agl_bg_colour_rbga
syn keyword aglFunction    agl_rect
syn keyword aglFunction    agl_rect_
syn keyword aglFunction    agl_irect
syn keyword aglFunction    agl_textured_rect
syn keyword aglFunction    agl_box
syn keyword aglFunction    agl_push_clip
syn keyword aglFunction    agl_pop_clip
syn keyword aglFunction    agl_print_error
syn keyword aglFunction    agl_print_stack_depths
syn keyword aglFunction    agl_set_font
syn keyword aglFunction    agl_set_font_string
syn keyword aglFunction    agl_print
syn keyword aglFunction    agl_print_layout
syn keyword aglFunction    agl_print_with_cursor
syn keyword aglFunction    agl_print_with_background
syn keyword aglFunction    agl_power_of_two
syn keyword aglFunction    agl_rgba_to_float
syn keyword aglFunction    agl_scale
syn keyword aglFunction    agl_translate
syn keyword aglFunction    agl_translate_abs
syn keyword aglMacro       AGL_NEW

" fbo.h
syn keyword aglType        AGlFBO
syn keyword aglType        AGlFBOFlags
syn keyword aglFunction    agl_fbo_new
syn keyword aglFunction    agl_fbo_free
syn keyword aglFunction    agl_fbo_free0
syn keyword aglFunction    agl_fbo_set_size
syn keyword aglConstant    AGL_MAX_FBO_WIDTH
syn keyword aglConstant    AGL_FBO_HAS_STENCIL

" observable.h
syn keyword aglType        AGlVal
syn keyword aglType        AGlObservable
syn keyword aglType        AGlObservableFn
syn keyword aglFunction    agl_observable_new
syn keyword aglFunction    agl_observable_free
syn keyword aglFunction    agl_observable_set_int
syn keyword aglFunction    agl_observable_set_float
syn keyword aglFunction    agl_observable_subscribe
syn keyword aglFunction    agl_observable_subscribe_with_state
syn keyword aglFunction    agl_observable_unsubscribe

" behaviour.h
syn keyword aglType        AGlBehaviour
syn keyword aglType        AGlBehaviourClass

" material.h
syn keyword aglFunction    agl_use_material
syn keyword aglFunction    agl_aa_line_new
syn keyword aglFunction    agl_aa_line_set_line

" gtk-area.h
syn keyword aglConstant    AGL_TYPE_GTK_AREA
syn keyword aglMacro       AGL_GTK_AREA
syn keyword aglType        AGlGtkArea
syn keyword aglType        AGlGtkAreaClass
syn keyword aglFunction    agl_gtk_area_new

" event.h
syn keyword aglType        AGlEvent
syn keyword aglType        AGlEventButton
syn keyword aglType        AGlEventKey
syn keyword aglType        AGlEventFocus
syn keyword aglType        AGlEventMotion
syn keyword aglType        AGlEventCrossing
syn keyword aglConstant    AGL_KEY_PRESS

" simple_key.h
syn keyword aglType        AGlKey

" frameclock.h
syn keyword aglMacro       AGL_TYPE_FRAME_CLOCK

" transition.h
syn keyword aglType        WfAnimatable
syn keyword aglType        WfAnimActor

" Default highlighting
if version >= 508 || !exists("did_agl_syntax_inits")
  if version < 508
    let did_agl_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif
  HiLink aglType               Type
  HiLink aglFunction           Function
  HiLink aglMacro              Macro
  HiLink aglConstant           Constant
  HiLink aglBoolean            Boolean
  HiLink aglDebug              Debug
  delcommand HiLink
endif
