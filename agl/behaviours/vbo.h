/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2021-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */
#pragma once

#include "glib.h"
#include "agl/actor.h"

typedef struct {
   AGlBehaviour  behaviour;
   unsigned int  vbo;
   AGlQuadVertex (*data)[];
   int           len;
   bool          valid;
} VboBehaviour;

AGlBehaviourClass* vbo_behaviour_get_class ();

AGlBehaviour* vbo_behaviour ();
