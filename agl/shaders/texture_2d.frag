uniform sampler2D tex2d;
uniform vec4 fg_colour;

varying vec2 tex_coords;

void main (void)
{
   gl_FragColor = fg_colour * texture2D(tex2d, tex_coords);
}
