/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2013-2025 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */
#define __agl_utils_c__

#include "config.h"
#include <assert.h>
#include <glib.h>
#include "agl/debug.h"
#include "agl/shader.h"
#include "agl/utils.h"
#include "agl/transform.h"
#include "agl/fbo.h"
#include "text/pango.h"
#include "text/driver.h"
#include "text/renderer.h"
#include "text/roundedrect.h"

#include "shaders/shaders.c"
#include "agl/shader.c"

static gulong __enable_flags = 0;

#define GL_ENABLE_ALPHA_TEST   (1<<3)
#define GL_ENABLE_TEXTURE_RECT (1<<4)

static int _program = 0;
GLenum _wf_ge = 0;

static GLuint agl_compile_shader_text (GLenum shaderType, const char* text);
static GLuint agl_compile_shader_file (GLenum shaderType, const char* filename);

static bool font_is_scalable (PangoContext*, const char* font_name);

AGl _agl = {
	.pref_use_shaders = TRUE,
	.use_shaders = FALSE,    // not set until we an have active gl context based on the value of pref_use_shaders.
	.shaders = {
		.alphamap = &alphamap,
		.alphamap_split = &alphamap_split,
		.texture = &tex2d,
		.plain = &plain,
		.dotted = &dotted,
	},
	.debug_flags = AGL_DEBUG_ALL
};

static AGl* agl = NULL;


AGl*
agl_get_instance ()
{
	if(!agl){
		agl = &_agl;

		driver_init();
	}
	return agl;
}


#ifdef USE_GTK
static GdkGLContext* share_list = 0;
#endif

void
agl_free ()
{
#ifdef USE_GTK
	// remove the reference that was added by gdk_gl_context_new
	g_clear_pointer(&share_list, g_object_unref);
#endif

	driver_free();
	ops_free(renderer.current_builder);
}


/*   Returns a global GdkGLContext that can be used to share
 *   OpenGL display lists between multiple drawables with
 *   dynamic lifetimes.
 */
#ifdef USE_GTK
GdkGLContext*
agl_get_gl_context ()
{
	if(!share_list){
		GdkGLConfig* const config = gdk_gl_config_new_by_mode(GDK_GL_MODE_RGBA | GDK_GL_MODE_DOUBLE | GDK_GL_MODE_DEPTH);
		GdkPixmap* const pixmap = gdk_pixmap_new(0, 8, 8, gdk_gl_config_get_depth(config));
		gdk_pixmap_set_gl_capability(pixmap, config, 0);
		share_list = gdk_gl_context_new(gdk_pixmap_get_gl_drawable(pixmap), 0, TRUE, GDK_GL_RGBA_TYPE);
	}

	return share_list;
}
#endif


/*
 *  Cache glEnable state to reduce number of GL calls.
 */
void
agl_enable (gulong flags)
{
	if (flags & AGL_ENABLE_BLEND) {
		if (!(__enable_flags & AGL_ENABLE_BLEND)) {
			glEnable (GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		}
		__enable_flags |= AGL_ENABLE_BLEND;
	} else if (__enable_flags & AGL_ENABLE_BLEND) {
		glDisable (GL_BLEND);
		__enable_flags &= ~AGL_ENABLE_BLEND;
	}

#if 0
	if (__enable_flags & AGL_ENABLE_BLEND)      dbg(0, "blend is enabled.");
#endif
}


GLboolean
agl_shaders_supported ()
{
	agl_get_instance();

	const char* version = (const char*)glGetString(GL_VERSION);

	if(!version){
		pwarn("cannot get gl version. incorrect mode?");
		goto no_shaders;
	}

	if ((version[0] >= '2' || version[0] <= '4') && version[1] == '.') {

		// some hardware cannot support shaders and software fallbacks are too slow
		if(g_strrstr((char*)glGetString(GL_RENDERER), "Intel") && g_strrstr((char*)glGetString(GL_RENDERER), "945")){
			goto no_shaders;
		}

		agl->use_shaders = TRUE;
		return GL_TRUE;
	}

  no_shaders:
	agl->use_shaders = FALSE;
	return GL_FALSE;
}


void
agl_create_programs ()
{
	for(int i=0;i<AGL_N_SHADERS;i++){
		AGlShader* shader = agl->programs[i];
		if(shader && !shader->program){
			agl_create_program(shader);
		}
	}
}


void
agl_gl_init ()
{
	static gboolean done = FALSE;
	if (done++) return;

	agl_get_instance();
#ifndef USE_EPOXY
	agl_get_extensions();
#endif

	if(agl->pref_use_shaders && !agl_shaders_supported()){
		printf("gl shaders not supported. expect reduced functionality.\n");
	}
	AGL_DEBUG printf("GL_RENDERER = %s\n", (const char*)glGetString(GL_RENDERER));

	int version = 0;
	const char* _version = (const char*)glGetString(GL_VERSION);
	if (_version) {

		GLint major, minor;
		int scanf_count = sscanf(_version, "%i.%i", &major, &minor);
		if (scanf_count != 2) {
			pwarn("failed to parse gl version");
		}
		dbg(1, "gl_version=%i.%i", major, minor);
		int factor = minor >= 10 ? 100 : 10;

		int r = factor * major + minor;
		dbg(1, "r=%i", r);
		if (r >= 30) {
			agl->have |= AGL_HAVE_3_0;
		}
		if (r >= 32) {
			agl->have |= AGL_HAVE_3_2;
		}
		version = r;
	}

	AGL_DEBUG printf("GLSL version %s\n", (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION));

	// npot textures are mandatory for opengl 2.0
	// npot capability also means non-square textures are supported.
	// some older hardware (eg radeon x1600) may not have full support,
	// and may drop back to software rendering if certain features are used.
	if (GL_ARB_texture_non_power_of_two || version > 10) {
		AGL_DEBUG printf("non_power_of_two textures are available.\n");
		agl->have |= AGL_HAVE_NPOT_TEXTURES;
	} else {
		AGL_DEBUG {
			fprintf(stderr, "GL_ARB_texture_non_power_of_two extension is not available!\n");
			fprintf(stderr, "Framebuffer effects will be lower resolution (lower quality).\n\n");
		}
	}

	if (agl->xvinfo) {
		int value;
#define _GET_CONFIG(__attrib) glXGetConfig (agl->xdisplay, agl->xvinfo, __attrib, &value)

		// Has stencil buffer?
		_GET_CONFIG (GLX_STENCIL_SIZE);
		if (value) agl->have |= AGL_HAVE_STENCIL;
	}

	if (!glBindVertexArray) {
		perr("vertex arrays unavailable");
	}

	glGenVertexArrays(1, &agl->vao);
	glGenBuffers(1, &agl->vbo);
	agl_use_va (agl->vao);

	renderer_init();
}


/*
 *  The purpose of this to help with memory leak detection,
 *  though these calls do not in practice make any difference.
 */
void
agl_gl_uninit ()
{
	glBindBuffer (GL_ARRAY_BUFFER, agl->vbo);
	glBufferData (GL_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);
#ifdef HAVE_GL_4_2
	glInvalidateBufferData (agl->vbo);
#endif
	glBindBuffer (GL_ARRAY_BUFFER, 0);
	glDeleteBuffers (1, &agl->vbo);
	glDeleteVertexArrays (1, &agl->vao);
}


static void
agl_uniforms_init (GLuint program, AGlUniformInfo uniforms[])
{
	dbg(1, "program=%u", program);
	if(!uniforms) return;

	for (GLuint i = 0; uniforms[i].name; i++) {
		uniforms[i].location = glGetUniformLocation(program, uniforms[i].name);
		// note zero is a valid location number.
		if(uniforms[i].location < 0) pwarn("%s: location=%i", uniforms[i].name, uniforms[i].location);

		switch (uniforms[i].size) {
			case 1:
				if (uniforms[i].type == GL_INT)
					glUniform1i(uniforms[i].location, (GLint) uniforms[i].value[0]);
				else
					glUniform1fv(uniforms[i].location, 1, uniforms[i].value);
				break;
			case 2:
				glUniform2fv(uniforms[i].location, 1, uniforms[i].value);
				break;
			case 3:
				glUniform3fv(uniforms[i].location, 1, uniforms[i].value);
				break;
			case 4:
				glUniform4fv(uniforms[i].location, 1, uniforms[i].value);
				break;
			default:
				abort();
		}
	}
}


GLuint
agl_create_program (AGlShader* sh)
{
	GLuint vert_shader = sh->vertex_file
		? agl_compile_shader_file(GL_VERTEX_SHADER, sh->vertex_file)
		: agl_compile_shader_text(GL_VERTEX_SHADER, sh->text->vert);
	GLuint frag_shader = sh->fragment_file
		? agl_compile_shader_file(GL_FRAGMENT_SHADER, sh->fragment_file)
		: agl_compile_shader_text(GL_FRAGMENT_SHADER, sh->text->frag);

	GLint status;
	glGetShaderiv(frag_shader, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE) {
		printf("shader compile error! %i\n", status);
		return 0;
	}

	GLuint program = sh->program = agl_link_shaders(vert_shader, frag_shader);
	dbg(2, "%u %u program=%u", vert_shader, frag_shader, program);

	glUseProgram(program);
	_program = program;

	agl_uniforms_init(program, sh->uniforms);

	return program;
}


static GLuint
agl_compile_shader_text (GLenum shaderType, const char* text)
{
	GLint stat;

	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, (const GLchar**) &text, NULL);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &stat);
	if (!stat) {
		GLchar log[1000];
		GLsizei len;
		glGetShaderInfoLog(shader, 1000, &len, log);
		g_error("problem compiling shader: '%s'\n", log);
		return 0;
	}
	return shader;
}


/**
 * Read a shader from a file.
 */
static GLuint
agl_compile_shader_file (GLenum shaderType, const char* filename)
{
   const int max = 100*1000;
   GLuint shader = 0;

   gchar* local_path = g_build_filename("shaders", filename, NULL);
   FILE* f = fopen(local_path, "r");
   g_free(local_path);
   if (!f) {
      //try installed file...
      gchar* path = g_build_filename(PACKAGE_DATA_DIR, PACKAGE, "shaders", filename, NULL);
      f = fopen(path, "r");
      g_free(path);

      if (!f) {
         pwarn("unable to open shader file %s", filename);
         return 0;
      }
   }

   char* buffer = (char*) malloc(max);
   int n = fread(buffer, 1, max, f);
   if (n > 0) {
      buffer[n] = 0;
      shader = agl_compile_shader_text(shaderType, buffer);
   }

   fclose(f);
   free(buffer);

   return shader;
}


GLuint
agl_link_shaders (GLuint vertShader, GLuint fragShader)
{
   GLuint program = glCreateProgram();

   assert(vertShader || fragShader);

   if (fragShader)
      glAttachShader(program, fragShader);
   if (vertShader)
      glAttachShader(program, vertShader);
   glLinkProgram(program);

   /* check link */
   {
      GLint stat;
      glGetProgramiv(program, GL_LINK_STATUS, &stat);
      if (!stat) {
         GLchar log[1000];
         GLsizei len;
         glGetProgramInfoLog(program, 1000, &len, log);
         fprintf(stderr, "Shader link error:\n%s\n", log);
         return 0;
      }
   }

   return program;
}


#if 0
void
agl_colour_rbga (uint32_t colour)
{
	float r = (colour & 0xff000000) >> 24;
	float g = (colour & 0x00ff0000) >> 16;
	float b = (colour & 0x0000ff00) >>  8;
	float a = (colour & 0x000000ff);

	glColor4f(r / 256.0, g / 256.0, b / 256.0, a / 256.0);
}
#endif


void
agl_bg_colour_rbga (uint32_t colour)
{
	float r = (colour & 0xff000000) >> 24;
	float g = (colour & 0x00ff0000) >> 16;
	float b = (colour & 0x0000ff00) >>  8;
	float a = (colour & 0x000000ff);

	glClearColor(r / 256.0, g / 256.0, b / 256.0, a / 256.0);
}


void
agl_rect (float x, float y, float w, float h)
{
	AGlfPt vertex[] = {
		{ x,     y     },
		{ x + w, y     },
		{ x,     y + h },
		{ x,     y + h },
		{ x + w, y     },
		{ x + w, y + h },
	};

	glBindBuffer (GL_ARRAY_BUFFER, agl->vbo);
	glBufferData (GL_ARRAY_BUFFER, sizeof(vertex), vertex, GL_STATIC_DRAW);

	glEnableVertexAttribArray (0);
	glVertexAttribPointer (0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glDrawArrays (GL_TRIANGLES, 0, G_N_ELEMENTS(vertex));
}


void
agl_rect_ (AGlRect r)
{
	AGlfPt vertex[] = {
		{ r.x,       r.y       },
		{ r.x + r.w, r.y       },
		{ r.x,       r.y + r.h },
		{ r.x,       r.y + r.h },
		{ r.x + r.w, r.y       },
		{ r.x + r.w, r.y + r.h },
	};

	glBindBuffer (GL_ARRAY_BUFFER, agl->vbo);
	glBufferData (GL_ARRAY_BUFFER, sizeof(vertex), vertex, GL_STATIC_DRAW);

	glEnableVertexAttribArray (0);
	glVertexAttribPointer (0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glDrawArrays (GL_TRIANGLES, 0, G_N_ELEMENTS(vertex));
}


/*
 *  Like agl_rect, but uses GL_QUADS instead of GL_TRIANGLES
 *  In general this is supposed to be less efficient on the GPU.
 */
void
agl_quad (float x, float y, float w, float h)
{
	AGlfPt vertex[] = {
		{ x,     y     },
		{ x + w, y     },
		{ x + w, y + h },
		{ x,     y + h },
	};

	glBindBuffer (GL_ARRAY_BUFFER, agl->vbo);
	glBufferData (GL_ARRAY_BUFFER, sizeof(vertex), vertex, GL_STATIC_DRAW);

	glEnableVertexAttribArray (0);
	glVertexAttribPointer (0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glDrawArrays (GL_QUADS, 0, G_N_ELEMENTS(vertex));
}


/*
 *  To use the whole texture, pass NULL for _t
 */
void
agl_textured_rect (guint texture, float x, float y, float w, float h, AGlQuad* _t)
{
	agl_use_texture(texture);

	AGlQuad t = _t ? *_t : (AGlQuad){0.0, 0.0, 1.0, 1.0};

	float vertices[] = { 
		// position    // texture coord
		x,     y + h,  t.x0, t.y1,
		x + w, y,      t.x1, t.y0,
		x,     y,      t.x0, t.y0, 

		x,     y + h,  t.x0, t.y1,
		x + w, y + h,  t.x1, t.y1,
		x + w, y,      t.x1, t.y0
	};

	agl_use_va(agl->vao);
	glBindBuffer(GL_ARRAY_BUFFER, agl->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);  

	glDrawArrays(GL_TRIANGLES, 0, 6);
}


void
agl_textured_rect_fast (guint texture, float x, float y, float w, float h, AGlQuad* _t)
{
	agl_use_texture(texture);

	AGlQuad t = _t ? *_t : (AGlQuad){0.0, 0.0, 1.0, 1.0};

	float vertices[] = { 
		// position    // texture coord
		x,     y + h,  t.x0, t.y1,
		x + w, y,      t.x1, t.y0,
		x,     y,      t.x0, t.y0, 

		x,     y + h,  t.x0, t.y1,
		x + w, y + h,  t.x1, t.y1,
		x + w, y,      t.x1, t.y0
	};

	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glDrawArrays(GL_TRIANGLES, 0, 6);
}


typedef struct {
	float x1, y1, x2, y2, x3, y3;
} Triangle;

static void
_set_quad (float vertices[], int i, float x, float y, float w, float h)
{
	*((Triangle*)&vertices[i * 12 + 0]) = (Triangle){x, y + h, x + w, y, x, y};
	*((Triangle*)&vertices[i * 12 + 6]) = (Triangle){x, y + h, x + w, y + h, x + w, y};
}


/*
 *   Box outline
 *   The dimensions specify the outer size.
 */
void
agl_box (int s, float x, float y, float w, float h)
{
	#define N_VERTICES 24
	float vertices[N_VERTICES * 2 + 2];

	_set_quad (vertices, 0, x,         y,         w, s        ); // top
	_set_quad (vertices, 1, x,         y + s,     s, h - 2 * s); // left
	_set_quad (vertices, 2, x,         y + h - s, w, s        ); // bottom
	_set_quad (vertices, 3, x + w - s, y + s,     s, h - 2 * s); // right

	glBindBuffer(GL_ARRAY_BUFFER, agl->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);  

	glDrawArrays(GL_TRIANGLES, 0, N_VERTICES);

	#undef N_VERTICES
}


static PangoFontDescription* font_desc = NULL;


void
agl_set_font (char* family, int size, PangoWeight weight)
{
	PangoContext* context = agl_pango_get_context();

	if(font_desc) pango_font_description_free(font_desc);

	font_desc = pango_font_description_new();
	pango_font_description_set_family(font_desc, family);
	pango_font_description_set_size(font_desc, size * PANGO_SCALE);
	pango_font_description_set_weight(font_desc, weight);

	// for some reason there seems to be an issue with pixmap fonts
	if(!font_is_scalable(context, pango_font_description_get_family(font_desc))){
		pango_font_description_set_family(font_desc, family);
	}
}


void
agl_set_font_string (char* font_string)
{
	if(font_desc) pango_font_description_free(font_desc);
	font_desc = pango_font_description_from_string(font_string);

	dbg(2, "%s", font_string);
	// for some reason there seems to be an issue with pixmap fonts
	if(!font_is_scalable(agl_pango_get_context(), pango_font_description_get_family(font_desc))){
		pango_font_description_set_family(font_desc, "Sans");
	}
}


void
agl_print (int x, int y, double z, uint32_t colour, const char* fmt, ...)
{
	if(!fmt) return;

	va_list args;
	va_start(args, fmt);
	gchar* text = g_strdup_vprintf(fmt, args);
	va_end(args);

#if 0
	if(!PGRC->context){
		PangoFontMap* fontmap = pango_gl_font_map_new();
		//pango_gl_font_map_set_resolution (PANGO_GL_FONT_MAP(fontmap), 96.0);
		PGRC->context = pango_gl_font_map_create_context(PANGO_GL_FONT_MAP(fontmap));
	}
#endif

	PangoLayout* layout = pango_layout_new (agl_pango_get_context ());
	pango_layout_set_text (layout, text, -1);
	g_free(text);

#if 0
	if(!font_desc){
		char font_string[64];
		get_font_string(font_string, -3);
		font_desc = pango_font_description_from_string(font_string);

		if(!font_is_scalable(PGRC->context, pango_font_description_get_family(font_desc))){
			strcpy(font_string, "Sans 7");
			pango_font_description_free(font_desc);
			font_desc = pango_font_description_from_string(font_string);
		}
		dbg(2, "%s", font_string);

		pango_font_description_set_weight(font_desc, PANGO_WEIGHT_SEMIBOLD); //TODO
	}
#endif

	pango_layout_set_font_description(layout, font_desc);

#if 0
	PangoFontMap* fontmap = pango_context_get_font_map (_context);
	PangoRenderer* renderer = pango_gl_font_map_get_renderer (PANGO_GL_FONT_MAP (fontmap));
#endif

	agl_pango_show_layout (layout, x, y, z, colour);

	g_object_unref(layout);
}


/*
 *  A variation of agl_print that updates the y position after printing
 */
void
agl_print_with_cursor (int x, int* y, double z, uint32_t colour, const char* fmt, ...)
{
	if(!fmt) return;

	va_list args;
	va_start(args, fmt);
	gchar* text = g_strdup_vprintf(fmt, args);
	va_end(args);

	PangoLayout* layout = pango_layout_new (agl_pango_get_context());
	pango_layout_set_text (layout, text, -1);
	g_free(text);

	agl_print_layout(x, *y, z, colour, layout);

	PangoRectangle irect = {0,}, lrect = {0,};
	pango_layout_get_pixel_extents (layout, &irect, &lrect);

	g_object_unref(layout);

	*y += irect.height;
}


void
agl_print_layout (int x, int y, double z, uint32_t colour, PangoLayout* layout)
{
	pango_layout_set_font_description(layout, font_desc);

	agl_pango_show_layout (layout, x, y, z, colour);
}


/**
 *  x, and y specify the top left corner of the background box.
 */
void
agl_print_with_background (int x, int y, double z, uint32_t colour, uint32_t bg_colour, const char *fmt, ...)
{
	#define PADDING 2

	if(!fmt) return;

	va_list args;
	va_start(args, fmt);
	gchar* text = g_strdup_vprintf(fmt, args);
	va_end(args);

	PangoLayout* layout = pango_layout_new (agl_pango_get_context());
	pango_layout_set_text (layout, text, -1);

	pango_layout_set_font_description(layout, font_desc);

	PangoRectangle ink_rect, logical_rect;
	pango_layout_get_pixel_extents(layout, &ink_rect, &logical_rect);
#if 0
	int text_width = ink_rect.width;
	int text_height = ink_rect.height;
	printf("   %i x %i  %i x %i\n", ink_rect.x, ink_rect.y, text_width, text_height);
#endif

	SET_PLAIN_COLOUR (agl->shaders.plain, bg_colour);
	agl_use_program (agl->shaders.plain);
	agl_translate (agl->shaders.plain, 0., 0.);
	agl_scale (agl->shaders.plain, 1., 1.);
	agl_rect(x, y, ink_rect.width + 2 * PADDING, ink_rect.height + 2 * PADDING);

	agl_pango_show_layout (layout, x + PADDING, y + PADDING - ink_rect.y, z, colour);

	g_object_unref(layout);
	g_free(text);
}


/*
 *  Scalable fonts dont list sizes, so if the font has a size list, we assume it is not scalable.
 *  TODO surely there is a better way to find a font than iterating over every system font?
 */
static bool
font_is_scalable (PangoContext* context, const char* font_name)
{
	g_return_val_if_fail(context, FALSE);
	g_return_val_if_fail(font_name, FALSE);

	bool scalable = TRUE;

	gchar* family_name = g_ascii_strdown(font_name, -1);
	dbg(3, "looking for: %s", family_name);

	PangoFontMap* fontmap = pango_context_get_font_map(context);
	PangoFontFamily** families = NULL;
	int n_families = 0;
	pango_font_map_list_families(fontmap, &families, &n_families);
	int i; for(i=0;i<n_families;i++){
		PangoFontFamily* family = families[i];
		if(!strcmp(family_name, pango_font_family_get_name(family))){
			PangoFontFace** faces;
			int n_faces;
			pango_font_family_list_faces(family, &faces, &n_faces);
			int j; for(j=0;j<n_faces;j++){
				PangoFontFace* face = faces[j];
				dbg(3, " %s", pango_font_face_get_face_name(face));
				int* sizes = NULL;
				int n_sizes = 0;
				pango_font_face_list_sizes(face, &sizes, &n_sizes);
				if(n_sizes){
					int i; for(i=0;i<n_sizes;i++){
						scalable = FALSE;
						break;
					}
					g_free(sizes);
				}
			}
			if(faces) g_free(faces);
		}
	}
	g_free(families);
	g_free(family_name);
	dbg(3, "scalable=%i", scalable);
	return scalable;
}


void
agl_use_program (AGlShader* shader)
{
	const int program = shader ? shader->program : 0;

	if(!agl_get_instance()->use_shaders) return;

	if(program != _program){
		dbg(3, "%i", program);
		glUseProgram(_program = program);
	}

	agl_scale (shader, 1., 1.);
	agl_translate (shader, 0., 0.);

	if(shader && shader->set_uniforms_) shader->set_uniforms_(shader);
}


void
agl_use_program_id (int id)
{
	if(!agl_get_instance()->use_shaders) return;

	if(id != _program){
		dbg(3, "%i", id);
		glUseProgram(_program = id);
	}
}


/*
 *  Set the current 2D texture
 */
void
agl_use_texture (GLuint texture)
{

	agl_enable(AGL_ENABLE_BLEND);

#if 0 // use this to check if texture gets out of sync
	GLint id;
	glGetIntegerv(GL_TEXTURE_BINDING_2D, &id);
#endif

	static GLuint _texture = -1;

	if (texture != _texture) {
		glBindTexture(GL_TEXTURE_2D, texture);
		_texture = texture;
	}
}


void
agl_use_va (GLuint va)
{
	static GLuint _va = -1;

	if (va != _va) {
		glBindVertexArray(va);
		_va = va;
	}
}


void
agl_push_clip (float x, float y, float w, float h)
{
	ops_push_clip (
		builder(),
		&AGL_ROUNDED_RECT_INIT(
			builder()->offset.x + x,
			builder()->offset.y + y,
			w,
			h
		)
	);
}


void
agl_pop_clip ()
{
	ops_pop_clip (builder());
}


void
agl_scale (AGlShader* shader, float x, float y)
{
	AGlfPt scale = builder()->target->width ? (AGlfPt){
		builder()->target->width / (2. * x),
		builder()->target->height / (2. * y)
	} : (AGlfPt){x, y};

	if (scale.x != shader->modelview.state[0] || scale.y != shader->modelview.state[1]) {
		glUniform2f (MODELVIEW, scale.x, scale.y);
		shader->modelview.state[0] = scale.x;
		shader->modelview.state[1] = scale.y;
	}
}


/*
 *  Apply a temporary translation during painting.
 *
 *  Note that consecutive calls are not cumulative.
 */
void
agl_translate (AGlShader* shader, float x, float y)
{
	AGlfPt pt = builder()->offset;
	agl_translate_abs (shader, pt.x + x, pt.y + y);
}


void
agl_translate_abs (AGlShader* shader, float x, float y)
{
	if (x != shader->translate.state[0] || y != shader->translate.state[1]) {
		glUniform2f (TRANSLATE, x, y);
		shader->translate.state[0] = x;
		shader->translate.state[1] = y;
	}
}


int
agl_power_of_two (guint a)
{
	// return the next power of two up from the given value.

	int i = 0;
#ifdef DEBUG
	int orig = a;
#endif
	a = MAX(1, a - 1);
	while(a){
		a = a >> 1;
		i++;
	}
	dbg (3, "%i -> %i", orig, 1 << i);
	return 1 << i;
}


void
agl_print_error (const char* func, int __ge, const char* format, ...)
{
	char str[256];
	char* e_str = NULL;

	switch(__ge) {
		case GL_INVALID_OPERATION:
			e_str = "GL_INVALID_OPERATION";
			break;
		case GL_INVALID_VALUE:
			e_str = "GL_INVALID_VALUE";
			break;
		case GL_INVALID_ENUM:
			e_str = "GL_INVALID_ENUM";
			break;
		case GL_STACK_OVERFLOW:
			e_str = "GL_STACK_OVERFLOW ";
			break;
		case GL_OUT_OF_MEMORY:
			e_str = "GL_OUT_OF_MEMORY";
			break;
		case GL_STACK_UNDERFLOW:
			e_str = "GL_STACK_UNDERFLOW";
			break;
		case GL_NO_ERROR:
			e_str = "GL_NO_ERROR";
			break;
		default:
			fprintf(stderr, "%i ", __ge); //TODO
			break;
	}

    va_list args;
    va_start(args, format);
	vsprintf(str, format, args);
    va_end(args);

	g_warning("%s(): %s %s", func, e_str, str);
}


void
agl_print_stack_depths ()
{
	GLint depth, max;
	printf("stack depths:\n");
	glGetIntegerv(GL_MODELVIEW_STACK_DEPTH, &depth);
	glGetIntegerv(GL_MAX_MODELVIEW_STACK_DEPTH, &max);
	printf("    ModelView:  %i / %i\n", depth, max);
	glGetIntegerv(GL_PROJECTION_STACK_DEPTH , &depth);
	glGetIntegerv(GL_MAX_PROJECTION_STACK_DEPTH, &max);
	printf("    Projection: %i / %i\n", depth, max);
	glGetIntegerv(GL_ATTRIB_STACK_DEPTH, &depth);
	glGetIntegerv(GL_MAX_ATTRIB_STACK_DEPTH, &max);
	printf("    Attribute:  %i / %i\n", depth, max);
}


void
agl_rgba_to_float (uint32_t rgba, float* r, float* g, float* b)
{
	double _r = (rgba & 0xff000000) >> 24;
	double _g = (rgba & 0x00ff0000) >> 16;
	double _b = (rgba & 0x0000ff00) >>  8;

	*r = _r / 0xff;
	*g = _g / 0xff;
	*b = _b / 0xff;
	dbg (3, "%08x --> %.2f %.2f %.2f", rgba, *r, *g, *b);
}


void
agl_set_colour_uniform (AGlUniformInfo* uniform, uint32_t rgba)
{
	AGlUniformUnion* u = (AGlUniformUnion*)uniform;

	if (rgba != uniform->state[0]) {
		float colour[4] = {0., 0., 0., ((float)(rgba & 0xff)) / 0x100};
		agl_rgba_to_float(rgba, &colour[0], &colour[1], &colour[2]);
		glUniform4fv(uniform->location, 1, colour);

		u->state.i[0] = u->value.i[0] = rgba;
	}
}


static AGlTextureUnit* active_texture_unit = NULL;


AGlTextureUnit*
agl_texture_unit_new (GLenum unit)
{
	AGlTextureUnit* texture_unit = g_new0(AGlTextureUnit, 1);
	texture_unit->unit = unit;
	return texture_unit;
}


void
agl_texture_unit_use_texture (AGlTextureUnit* unit, int texture)
{
	g_return_if_fail(unit);

	if(TRUE || active_texture_unit != unit){
		active_texture_unit = unit;
		glActiveTexture(unit->unit);
	}
	glBindTexture(GL_TEXTURE_1D, texture);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
#if 1
	agl_enable(AGL_ENABLE_BLEND);
	/*
	if(!(__enable_flags & AGL_ENABLE_BLEND)){
		agl_enable(GL_BLEND);
	}
	*/
#else
	glEnable(GL_BLEND);
#endif
}


/*
 *  Sets a single quad at position i in the given vertex array
 */
void
agl_set_quad (AGlQuadVertex (*v)[], int i, AGlVertex p0, AGlVertex p1)
{
	(*v)[i] = (AGlQuadVertex){
		(AGlVertex){p0.x, p0.y},
		(AGlVertex){p1.x, p0.y},
		(AGlVertex){p1.x, p1.y},
		(AGlVertex){p0.x, p0.y},
		(AGlVertex){p1.x, p1.y},
		(AGlVertex){p0.x, p1.y},
	};
}


#ifdef DEBUG
void
agl_marker (char* str)
{
#if 0
	glStringMarkerGREMEDY(0, str);
#endif
	g_free (str);
}
#endif
