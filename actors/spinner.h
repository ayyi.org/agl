/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2012-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#pragma once

#include "agl/actor.h"

typedef struct {
    AGlActor       actor;
	bool           spinning;
} AGlSpinner;

AGlActor* agl_spinner       (void*);
void      agl_spinner_start (AGlSpinner*);
void      agl_spinner_stop  (AGlSpinner*);
