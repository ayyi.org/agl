/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2012-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#undef USE_GTK
#include <sys/time.h>
#include "agl/actor.h"
#include "debug/debug.h"
#include "agl/shader.h"
#include "actors/texture.h"

static AGl* agl = NULL;

static AGlActorClass actor_class = {0, "Texture", (AGlActorNew*)texture_node, texture_free};


static void
texture_set_state (AGlActor* actor)
{
	((AlphaMapShader*)actor->program)->uniform.fg_colour = actor->colour;
}


static bool
texture_paint (AGlActor* actor)
{
	agl_textured_rect(
		((AGlTextureNode*)actor)->texture,
		0,
		0,
		agl_actor__width (actor),
		agl_actor__height (actor),
		NULL
	);

	return true;
}


void
texture_free (AGlActor* actor)
{
	AGlTextureNode* ta = (AGlTextureNode*)actor;

	if (!actor->class->instances) {
		glDeleteTextures(1, &ta->texture);
		ta->texture = 0;
	}

	g_free(actor);
}


AGlActor*
texture_node (void* _)
{
	agl = agl_get_instance ();

	return (AGlActor*) agl_actor__new (AGlTextureNode, {
		.class = &actor_class,
		.program = (AGlShader*)agl->shaders.texture,
		.set_state = texture_set_state,
		.paint = texture_paint,
		.colour = 0xffffffff
	});
}
