/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2018-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include <glib.h>
#include "agl/utils.h"
#include "observable.h"

typedef struct {
   AGlObservableFn fn;
   gpointer        user;
} Subscription;


AGlObservable*
agl_observable_new ()
{
	return AGL_NEW(AGlObservable,
		.max.i = INT_MAX
	);
}


/*
 *  This will not free string values. Freeing of string values needs to be done by the user.
 */
void
agl_observable_free (AGlObservable* observable)
{
	g_list_free_full(observable->subscriptions, g_free);
	g_free(observable);
}


void
agl_observable_set_int (AGlObservable* observable, int value)
{
	value = CLAMP(value, observable->min.i, observable->max.i);

	if (value != observable->value.i) {
		observable->value.i = value;

		GList* l = observable->subscriptions;
		for(;l;l=l->next){
			Subscription* subscription = l->data;
			subscription->fn(observable, observable->value, subscription->user);
		}
	}
}


void
agl_observable_set_float (AGlObservable* observable, float value)
{
	if(value >= observable->min.f && value <= observable->max.f){

		observable->value.f = value;

		GList* l = observable->subscriptions;
		for(;l;l=l->next){
			Subscription* subscription = l->data;
			subscription->fn(observable, observable->value, subscription->user);
		}
	}
}


void
agl_observable_subscribe (AGlObservable* observable, AGlObservableFn fn, gpointer user)
{
	observable->subscriptions = g_list_append(observable->subscriptions, AGL_NEW(Subscription,
		.fn = fn,
		.user = user
	));
}


/*
 *  Calls back imediately with the current value
 */
void
agl_observable_subscribe_with_state (AGlObservable* observable, AGlObservableFn fn, gpointer user)
{
	agl_observable_subscribe(observable, fn, user);
	fn(observable, observable->value, user);
}


/*
 *  Disconnect by either fn or user_data if they are set.
 *  If both are set, both must match
 */
void
agl_observable_unsubscribe (AGlObservable* observable, AGlObservableFn fn, gpointer user)
{
	for(GList* l=observable->subscriptions;l;){
		Subscription* subscription = l->data;
		GList* link = l;
		l = l->next;
		if((!fn || fn == (subscription->fn)) && (!user || (user == subscription->user))){
			g_free(subscription);
			observable->subscriptions = g_list_delete_link(observable->subscriptions, link);
		}
	}
}


