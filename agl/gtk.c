/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

static void     agl_actor__on_unrealise (GtkWidget*, gpointer actor);
static void     agl_actor__on_realise   (GtkWidget*, gpointer actor);
static gboolean agl_actor__try_drawable (gpointer actor);


AGlActor*
agl_new_scene_gtk (GtkWidget* widget)
{
	AGlScene* scene = (AGlRootActor*)agl_actor__new_root_(CONTEXT_TYPE_GTK);
	scene->gl.gdk.widget = widget;
	scene->user_data = widget;

	if(GTK_WIDGET_REALIZED(widget)){
		agl_actor__on_realise(widget, scene);
	}
	g_signal_connect((gpointer)widget, "realize", G_CALLBACK(agl_actor__on_realise), scene);
	g_signal_connect((gpointer)widget, "unrealize", G_CALLBACK(agl_actor__on_unrealise), scene);

	g_signal_connect_swapped((gpointer)widget, "event", G_CALLBACK(agl_actor__on_event), scene);

	return (AGlActor*)scene;
}


static void
agl_actor__on_unrealise (GtkWidget* widget, gpointer _actor)
{
	AGlRootActor* a = _actor;

	a->gl.gdk.drawable = NULL;
	a->gl.gdk.context  = NULL;
}


static void
agl_actor__on_realise (GtkWidget* widget, gpointer _actor)
{
	AGlRootActor* a = _actor;

	agl_actor__try_drawable(a);
	if(!a->gl.gdk.drawable){
		g_idle_add(agl_actor__try_drawable, a);
	}
}


static void
agl_actor__have_drawable (AGlRootActor* a, GdkGLDrawable* drawable)
{
	g_return_if_fail(!a->gl.gdk.drawable);
	g_return_if_fail(!a->gl.gdk.context);

	static bool first_time = true;

	((AGlActor*)a)->scrollable.x2 = a->gl.gdk.widget->allocation.width;
	((AGlActor*)a)->scrollable.y2 = a->gl.gdk.widget->allocation.height;

	a->gl.gdk.drawable = drawable;
	a->gl.gdk.context = agl_get_gl_context();

#ifdef USE_SYSTEM_GTKGLEXT
	gdk_gl_drawable_make_current (a->gl.gdk.drawable, a->gl.gdk.context);
#else
	g_assert(G_OBJECT_TYPE(a->gl.gdk.drawable) == GDK_TYPE_GL_WINDOW);
	gdk_gl_window_make_context_current (a->gl.gdk.drawable, a->gl.gdk.context);
#endif

	if(first_time){
		agl_gl_init();

#ifdef DEBUG
		if(agl->debug){
			int version = 0;
			const char* _version = (const char*)glGetString(GL_VERSION);
			if(_version){
				gchar** split = g_strsplit(_version, ".", 2);
				if(split){
					version = atoi(split[0]);
					printf("gl version: %i\n", version);
					g_strfreev(split);
				}
			}
		}
#endif
	}

	agl_actor__init((AGlActor*)a);

	first_time = false;
}


static gboolean
agl_actor__try_drawable (gpointer _actor)
{
	AGlRootActor* a = _actor;
	if(!a->gl.gdk.drawable){
		GdkGLDrawable* drawable = gtk_widget_get_gl_drawable(a->gl.gdk.widget);
		if(drawable){
			agl_actor__have_drawable(a, drawable);
		}
	}
	return G_SOURCE_REMOVE;
}


/*
 *  Utility function for use as a gtk "expose-event" handler.
 *  Application must pass the root actor as user_data when connecting the signal.
 */
#ifdef USE_GTK
bool
agl_actor__on_expose (GtkWidget* widget, GdkEventExpose* event, gpointer user_data)
{
	void set_projection (AGlActor* actor)
	{
		int vx = 0;
		int vy = 0;
		int vw = MAX(64, actor->region.x2);
		int vh = MAX(64, actor->region.y2);
		glViewport(vx, vy, vw, vh);
		AGL_DEBUG printf("viewport: %i %i %i %i\n", vx, vy, vw, vh);
	}

	AGlScene* root = user_data;
	g_return_val_if_fail(root, false);

	if(!GTK_WIDGET_REALIZED(widget)) return true;

	AGL_ACTOR_START_DRAW(root) {
		set_projection((AGlActor*)root);
		agl_bg_colour_rbga(root->bg_colour);
		glClear(GL_COLOR_BUFFER_BIT);

		agl_actor__paint((AGlActor*)root);

#undef SHOW_BOUNDING_BOX
#ifdef SHOW_BOUNDING_BOX
		glDisable(GL_TEXTURE_2D);

		int w = GL_WIDTH;
		int h = GL_HEIGHT/2;
		glBegin(GL_QUADS);
		glVertex3f(-0.2, -0.2, 1); glVertex3f(w, -0.2, 1);
		glVertex3f(w, h, 1);       glVertex3f(-0.2, h, 1);
		glEnd();
		glEnable(GL_TEXTURE_2D);
#endif

#if USE_SYSTEM_GTKGLEXT
		gdk_gl_drawable_swap_buffers(root->gl.gdk.drawable);
#else
		gdk_gl_window_swap_buffers(root->gl.gdk.drawable);
#endif
	} AGL_ACTOR_END_DRAW(root)

	return true;
}
#endif
