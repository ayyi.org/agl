/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#ifndef __agl_gtk_h__
#define __agl_gtk_h__

#include "agl/actor.h"

AGlActor* agl_new_scene_gtk    (GtkWidget*);
bool      agl_actor__on_expose (GtkWidget*, GdkEventExpose*, gpointer);

#endif
