/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2012-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __agl_shader_h__
#define __agl_shader_h__

#include "agl/utils.h"

struct _AlphamapShader {
	AGlShader    shader;
	struct {
		uint32_t fg_colour;
	}            uniform;
};

typedef enum {
	PLAIN_COLOUR = 0,
} PlainUniforms;

typedef enum {
	ROTATABLE_TEX = 0,
	ROTATABLE_COLOUR,
	ROTATABLE_ROTATION,
	ROTATABLE_ORIGIN,
} RotatableUniforms;

#ifdef __gl_h_
#define PLAIN_COLOUR2(SHADER) \
	(((AGlUniformUnion*)&(SHADER)->uniforms[PLAIN_COLOUR])->value.i[0])
#define SET_PLAIN_COLOUR(SHADER, COLOUR) \
	(((AGlUniformUnion*)&(SHADER)->uniforms[PLAIN_COLOUR])->value.i[0] = COLOUR)

typedef struct {
	AGlShader      shader;
	struct {
		AGliPt     centre1;
		AGliPt     centre2;
		float      radius;
	}              uniform;
} ScrollbarShader;

#define V_SCROLLBAR_COLOUR() \
	(((AGlUniformUnion*)&v_scrollbar_shader.shader.uniforms[0])->value.i[0])
#define V_SCROLLBAR_BG_COLOUR() \
	(((AGlUniformUnion*)&v_scrollbar_shader.shader.uniforms[1])->value.i[0])

#ifdef __agl_utils_c__
AGlUniformInfo agl_null_uniforms[] = {
   END_OF_UNIFORMS
};
#else
extern AGlUniformInfo agl_null_uniforms[];
#endif

#endif
#endif
