/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://ayyi.org              |
 | copyright (C) 2020-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#undef USE_GTK
#include <getopt.h>
#include "gdk/gdk.h"
#include "agl/x11.h"
#include "agl/debug.h"
#include "agl/text/text_node.h"
#include "agl/behaviours/key.h"
#include "actors/spinner.h"

extern AGlMaterialClass aaline_class;

#define N_LINES 16

static ActorKeyHandler
	start,
	stop;

static ActorKey keys[] = {
	{XK_equal,       start},
	{XK_KP_Add,      start},
	{XK_minus,       stop},
	{XK_KP_Subtract, stop},
	{XK_Escape,      (ActorKeyHandler*)exit},
	{XK_q,           (ActorKeyHandler*)exit},
	{0,}
};

static const struct option long_options[] = {
	{ "non-interactive",  0, NULL, 'n' },
};

static const char* const short_options = "n";


int
main (int argc, char* argv[])
{
	const int width = 400, height = 200;

	int opt;
	while ((opt = getopt_long (argc, argv, short_options, long_options, NULL)) != -1) {
		switch (opt) {
			case 'n':
				g_timeout_add(3000, (gpointer)exit, NULL);
				break;
		}
	}

	AGlWindow* window = agl_window("Text test", 0, 0, width, height, 0);
	XMapWindow(dpy, window->window);

	AGlActor* text = agl_actor__add_child((AGlActor*)window->scene, text_node(NULL));
	text_node_set_text((TextNode*)text, g_strdup("Spinner test"));
	text->colour = 0xbbbbbbff;
	text->region = (AGlfRegion){.x2 = 80, .y2 = 30};

	AGlActor* spinner = agl_actor__add_child((AGlActor*)window->scene, agl_spinner(NULL));
	((AGlSpinner*)spinner)->spinning = true;

	window->scene->selected = spinner;
	agl_actor__add_behaviour(spinner, ({
		AGlBehaviour* k = key_behaviour();
		((KeyBehaviour*)k)->keys = &keys;
		key_behaviour_init (k, spinner);
		k;
	}));

	if (g_getenv("NON_INTERACTIVE")) {
		g_timeout_add(3000, (gpointer)exit, NULL);

		gboolean run (void* spinner)
		{
			gboolean run (void* spinner)
			{
				stop(spinner, 0);
				return G_SOURCE_REMOVE;
			}
			start(spinner, 0);
			g_timeout_add(1000, (gpointer)run, spinner);
			return G_SOURCE_REMOVE;
		}
		g_timeout_add(1000, (gpointer)run, spinner);
	}

	g_main_loop_run(agl_main_loop_new());

	agl_window_destroy(&window);
	XCloseDisplay(dpy);

	return 0;
}


static bool
start (AGlActor* spinner, GdkModifierType m)
{
	((AGlSpinner*)spinner)->spinning = false;
	agl_spinner_start ((AGlSpinner*)spinner);
	return true;
}


static bool
stop (AGlActor* spinner, GdkModifierType m)
{
	agl_spinner_stop ((AGlSpinner*)spinner);
	((AGlSpinner*)spinner)->spinning = true; // make it visible
	return true;
}
