/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. https://www.ayyi.org          |
 | copyright (C) 2004-2022 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include <glib-object.h>
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <gtk/gtk.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include "debug/debug.h"
#include "agl/gtk.h"
#include "gtkglext-1.0/gtk/gtkgl.h"
#include "gtk-area.h"

extern GdkGLConfig* app_get_glconfig ();

#define DIRECT TRUE

static GdkGLConfig* glconfig = NULL;
static gpointer gl_area_parent_class = NULL;

enum  {
	GL_AREA_0_PROPERTY
};

static void     gl_area_realize         (GtkWidget*);
static void     gl_area_unrealize       (GtkWidget*);
static gboolean gl_area_configure_event (GtkWidget*, GdkEventConfigure*);
static gboolean gl_area_expose_event    (GtkWidget*, GdkEventExpose*);
static gboolean gl_area_event           (GtkWidget*, GdkEvent*);


GlArea*
gl_area_construct (GType object_type)
{
	GlArea* area = g_object_new (object_type, NULL);
	area->scene = (AGlScene*)agl_new_scene_gtk((GtkWidget*)area);

	return area;
}


GlArea*
gl_area_new (void)
{
	return gl_area_construct (TYPE_GL_AREA);
}


static void
gl_area_realize (GtkWidget* widget)
{
	gtk_widget_set_gl_capability(widget, glconfig, agl_get_gl_context(), DIRECT, GDK_GL_RGBA_TYPE);
	GTK_WIDGET_CLASS(gl_area_parent_class)->realize (widget);
}


static void
gl_area_unrealize (GtkWidget* widget)
{
	PF;
	GTK_WIDGET_CLASS(gl_area_parent_class)->unrealize (widget);
}


static void
gl_area_class_init (GlAreaClass* klass)
{
	gl_area_parent_class = g_type_class_peek_parent (klass);

	((GtkWidgetClass*)klass)->realize = gl_area_realize;
	((GtkWidgetClass*)klass)->unrealize = gl_area_unrealize;
	((GtkWidgetClass*)klass)->configure_event = gl_area_configure_event;
	((GtkWidgetClass*)klass)->expose_event = gl_area_expose_event;
	((GtkWidgetClass*)klass)->event = gl_area_event;

	glconfig = gdk_gl_config_new_by_mode (GDK_GL_MODE_RGBA | GDK_GL_MODE_DEPTH | GDK_GL_MODE_DOUBLE);
}


static void
gl_area_instance_init (GlArea* self)
{
}


GType
gl_area_get_type (void)
{
	static volatile gsize gl_area_type_id__volatile = 0;
	if (g_once_init_enter ((gsize*)&gl_area_type_id__volatile)) {
		static const GTypeInfo g_define_type_info = { sizeof (GlAreaClass), (GBaseInitFunc) NULL, (GBaseFinalizeFunc) NULL, (GClassInitFunc) gl_area_class_init, (GClassFinalizeFunc) NULL, NULL, sizeof (GlArea), 0, (GInstanceInitFunc) gl_area_instance_init, NULL };
		GType gl_area_type_id;
		gl_area_type_id = g_type_register_static (GTK_TYPE_DRAWING_AREA, "GlArea", &g_define_type_info, 0);
		g_once_init_leave (&gl_area_type_id__volatile, gl_area_type_id);
	}
	return gl_area_type_id__volatile;
}


static AGl* agl;

static gboolean
gl_area_configure_event (GtkWidget* widget, GdkEventConfigure* event)
{
	GlArea* self = (GlArea*)widget;

	GdkGLDrawable* gldrawable = g_object_ref (gtk_widget_get_gl_drawable (widget));

	if (!gdk_gl_drawable_make_current (gldrawable, agl_get_gl_context())) {
		g_object_unref (gldrawable);
		return FALSE;
	}

	if (!agl) {
		agl = agl_get_instance ();
		agl_gl_init ();
	}

	((AGlActor*)self->scene)->region = (AGlfRegion){0., 0., widget->allocation.width, widget->allocation.height};
	agl_actor__set_size ((AGlActor*)self->scene);

	g_object_unref (gldrawable);

	return TRUE;
}


static gboolean
gl_area_expose_event (GtkWidget* widget, GdkEventExpose* event)
{
	return agl_actor__on_expose (widget, event, ((GlArea*)widget)->scene);
}


static gboolean
gl_area_event (GtkWidget* widget, GdkEvent* event)
{
	g_return_val_if_fail (event, FALSE);

	return agl_actor__on_event (((GlArea*)widget)->scene, event);
}

