/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2012-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#define __glx_test__
#include "config.h"
#undef USE_GTK
#include <getopt.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include "gdk/gdk.h"
#include "agl/ext.h"
#include "agl/x11.h"
#include "agl/actor.h"
#include "test/common2.h"
#include "agl/shader.h"
#include "actors/plain.h"

extern char* basename (const char*);

AGlActor* texture_actor (void*);

static AGlScene* scene = NULL;
struct {AGlActor *l1, *l2; } layers = {0,};

static KeyHandler
	nav_up,
	nav_down,
	zoom_in,
	zoom_out;

Key keys[] = {
	{XK_Up,          nav_up},
	{XK_Down,        nav_down},
	{XK_equal,       zoom_in},
	{XK_KP_Add,      zoom_in},
	{XK_minus,       zoom_out},
	{XK_KP_Subtract, zoom_out},
};

static const struct option long_options[] = {
	{ "help",             0, NULL, 'h' },
	{ "non-interactive",  0, NULL, 'n' },
};

static const char* const short_options = "nh";

static const char* const usage =
	"Usage: %s [OPTIONS]\n\n"
	"\n"
	"Options:\n"
	"  --help\n"
	"  --non-interative\n"
	"\n";


#include "agl/text/driver.h"
GskQuadVertex vertex[12] = {
	{{  0.,   0.}},
	{{ 50.,   0.}},
	{{  0.,  50.}},
	{{  0.,  50.}},
	{{ 50.,   0.}},
	{{ 50.,  50.}},

	{{250.,   0.}},
	{{300.,   0.}},
	{{300.,  50.}},

	{{300., 250.}},
	{{300., 300.}},
	{{250., 300.}}
};


int
main (int argc, char* argv[])
{
	int width = 300, height = 300;

	if (g_getenv("NON_INTERACTIVE")) {
		g_timeout_add(3000, (gpointer)exit, NULL);
	}

	int opt;
	while ((opt = getopt_long (argc, argv, short_options, long_options, NULL)) != -1) {
		switch (opt) {
			case 'h':
				printf(usage, basename(argv[0]));
				exit(EXIT_SUCCESS);
				break;
			case 'n':
				g_timeout_add(3000, (gpointer)exit, NULL);
				break;
		}
	}

	AGlWindow* window = agl_window("agltexturetest", 0, 0, width, height, 0);
	scene = window->scene;

	{
		agl_actor__add_child((AGlActor*)scene, layers.l2 = texture_actor(NULL));
		layers.l2->colour = 0x9999ff99;
		layers.l2->region = (AGlfRegion){0., 0., 300., 300.};

		AGlActor* b = agl_actor__add_child((AGlActor*)scene, plain_actor(NULL));
		b->colour = 0x00ff0099;
		b->region = (AGlfRegion){10., 200., 60., 250.};

		// nested
		AGlActor* c = agl_actor__add_child(b, plain_actor(NULL));
		c->colour = 0xff990099;
		c->region = (AGlfRegion){25., 25., 75., 75.};
	}

	add_key_handlers(keys);

	g_main_loop_run(agl_main_loop_new());

	agl_window_destroy(&window);
	XCloseDisplay(dpy);

	return 0;
}


static void
nav_up (gpointer user_data)
{
	PF0;
}


static void
nav_down (gpointer user_data)
{
	PF0;
}


static void
zoom_in (gpointer user_data)
{
}


static void
zoom_out (gpointer user_data)
{
}


static GLuint textures[1] = {0,};

static void
create_rgb_texture ()
{
	glGenTextures(1, textures);

	int width = 256;
	int height = 256;
	char* pbuf = g_new0(char, width * height * 3);
	for (int y=0;y<height;y++) {
		for (int x=0;x<width;x++) {
			*(pbuf + y * (width*3) + (x*3))     = ((x+y) * 0xff) / (width * 2);
			*(pbuf + y * (width*3) + (x*3) + 1) = ((x+y) * 0xff) / (width * 2);
			*(pbuf + y * (width*3) + (x*3) + 2) = ((x+y) * 0xff) / (width * 2);
		}
	}

	#define pixel_format2 GL_RGB
	agl_use_texture (textures[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D   (GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0, pixel_format2, GL_UNSIGNED_BYTE, pbuf);
	gl_warn("bg texture bind");

	g_free(pbuf);
}


AGlActor*
texture_actor (void* view)
{
	static GLuint buffer_id;

	void texture_init (AGlActor* actor)
	{
		create_rgb_texture();

		glGenBuffers (1, &buffer_id);
	}

	void texture_set_state (AGlActor* actor)
	{
		((AlphaMapShader*)actor->program)->uniform.fg_colour = 0xff99ffff;
	}

	bool texture_paint (AGlActor* actor)
	{
		glBindBuffer (GL_ARRAY_BUFFER, buffer_id);
		glBufferData (GL_ARRAY_BUFFER, sizeof(vertex), vertex, GL_STATIC_DRAW);

		glEnableVertexAttribArray (0);
		glVertexAttribPointer (0, 2, GL_FLOAT, GL_FALSE, sizeof(GskQuadVertex), NULL);

		glDrawArrays (GL_TRIANGLES, 0, G_N_ELEMENTS(vertex));

		agl_textured_rect (textures[0], 100., 100., 50., 50., NULL);

		return true;
	}

	void texture_free ()
	{
		glDeleteBuffers (1, &buffer_id);
	}

	static AGlActorClass texture_class = {0, "Texture"};
	texture_class.free = texture_free;

	AGlActor* actor = AGL_NEW(AGlActor,
		.class = &texture_class,
		.name = "Texture",
		.init = texture_init,
		.region = {
			.x2 = 1, .y2 = 1 // must have size else will not be rendered
		},
		.set_state = texture_set_state,
		.paint = texture_paint,
		.program = (AGlShader*)agl_get_instance()->shaders.texture,
	);

	return actor;
}
