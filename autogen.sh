#!/bin/sh

echo 'Generating agl configuration files ...'
libtoolize --automake
aclocal
autoheader -Wall
automake --gnu --add-missing -Wall
autoconf

cd gtkglext-1.0 &&
	./autogen.sh
