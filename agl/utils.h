/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#pragma once

#include <stdbool.h>
#include "agl/ext.h"
#if defined(USE_GTK) || defined(__GTK_H__)
#include <gdk/gdkgl.h>
#include <gtk/gtkgl.h>
#endif
#include <pango/pango.h>
#include "agl/typedefs.h"
#include "agl/material.h"

typedef struct _agl            AGl;
typedef struct _AGlUniformInfo AGlUniformInfo;

typedef struct _agl_shader_text
{
	char*        vert;
	char*        frag;
} AGlShaderText;

struct _AGlUniformInfo
{
   const char* name;
   GLuint      size;
   GLenum      type;      // GL_FLOAT or GL_INT
   GLint       location;  // filled in by agl_create_program()
   GLfloat     value[4];
   GLfloat     state[4];
};

typedef struct
{
   const char* name;
   GLuint      size;
   GLenum      type;      // GL_FLOAT or GL_INT
   GLint       location;  // filled in by agl_create_program()
   union {
      float    f[4];
      uint32_t i[4];
   }           value;
   union {
      float    f[4];
      uint32_t i[4];
   }           state;
} AGlUniformUnion;

struct _AGlShader
{
	char*           vertex_file;
	char*           fragment_file;
	uint32_t        program;       // compiled program
	AGlUniformInfo* uniforms;
	void            (*set_uniforms_)(AGlShader*);
	AGlShaderText*  text;
	AGlUniformInfo  modelview;
	AGlUniformInfo  translate;
};

struct _texture_unit
{
   GLenum      unit;
   int         texture;
};

AGl*      agl_get_instance        ();
#if defined(USE_GTK) || defined(__GTK_H__)
GdkGLContext*
          agl_get_gl_context      ();
#endif
void      agl_enable              (gulong flags);
void      agl_gl_init             ();
void      agl_gl_uninit           ();
void      agl_free                ();
GLboolean agl_shaders_supported   ();
GLuint    agl_create_program      (AGlShader*);
GLuint    agl_link_shaders        (GLuint vertShader, GLuint fragShader);
void      agl_set_uniforms        (AGlShader*);
void      agl_set_colour_uniform  (AGlUniformInfo*, uint32_t);
void      agl_use_program         (AGlShader*);
void      agl_use_program_id      (int);
void      agl_use_texture         (GLuint texture);
void      agl_use_va              (GLuint);

AGlTextureUnit* agl_texture_unit_new         (GLenum unit);
void            agl_texture_unit_use_texture (AGlTextureUnit*, int texture);

void      agl_bg_colour_rbga      (uint32_t);

void      agl_rect                (float x, float y, float w, float h);
void      agl_quad                (float x, float y, float w, float h);
void      agl_rect_               (AGlRect);
void      agl_textured_rect       (guint texture, float x, float y, float w, float h, AGlQuad* tex_rect);
void      agl_textured_rect_fast  (guint texture, float x, float y, float w, float h, AGlQuad* tex_rect);
void      agl_box                 (int line_width, float x, float y, float w, float h);

void      agl_push_clip           (float x, float y, float w, float h);
void      agl_pop_clip            ();

void      agl_scale               (AGlShader*, float x, float y);
void      agl_translate           (AGlShader*, float x, float y);
void      agl_translate_abs       (AGlShader*, float x, float y);

void      agl_print_error         (const char* func, int err, const char* format, ...);
void      agl_print_stack_depths  ();

void      agl_set_font            (char* family, int size, PangoWeight);
void      agl_set_font_string     (char* font_string);
void      agl_print               (int x, int y, double z, uint32_t colour, const char* fmt, ...);
void      agl_print_layout        (int x, int y, double z, uint32_t colour, PangoLayout*);
void      agl_print_with_cursor   (int x, int* y, double z, uint32_t colour, const char* fmt, ...);
void      agl_print_with_background
                                  (int x, int y, double z, uint32_t colour, uint32_t bg_colour, const char* fmt, ...);

int       agl_power_of_two        (guint);
void      agl_rgba_to_float       (uint32_t rgba, float* r, float* g, float* b);

void      agl_set_quad            (AGlQuadVertex (*v)[], int i, AGlVertex, AGlVertex);

#ifdef DEBUG
void      agl_marker (char*);
#endif


typedef enum
{
    AGL_HAVE_NPOT_TEXTURES = 1,
    AGL_HAVE_STENCIL       = 2,
    AGL_HAVE_3_0           = 4,
    AGL_HAVE_3_2           = 8,
} AGlHave;

typedef enum
{
    AGL_TEXTURE_SHADER = 0,
    AGL_ALPHAMAP_SHADER,
    AGL_ALPHAMAP_SHADER_SPLIT,
    AGL_PLAIN_SHADER,
    AGL_DOTTED_SHADER,
    AGL_ROTATABLE_SHADER,
    AGL_APPLICATION_SHADER_1,
    AGL_APPLICATION_SHADER_2,
    AGL_APPLICATION_SHADER_3,
    AGL_APPLICATION_SHADER_4,
    AGL_APPLICATION_SHADER_5,
    AGL_N_SHADERS
} AGlShaders;

typedef enum
{
    MODELVIEW = 0,
    TRANSLATE
} AGlUniformLocation;

struct _agl
{
    bool            pref_use_shaders;

    XVisualInfo*    xvinfo;
    Display*        xdisplay;

    bool            use_shaders;
    AGlHave         have;

    union {
       AGlShader* programs[AGL_N_SHADERS];
       struct {
          AlphaMapShader* texture;
          AlphaMapShader* alphamap;
          AlphaMapShader* alphamap_split;
          AGlShader*      plain;
          AGlShader*      dotted;
       }            shaders;
    };

    AGlMaterial*    aaline;

    unsigned int    vao;
    unsigned int    vbo;

    int             debug;
    int             debug_flags;
};

#define END_OF_UNIFORMS   { NULL, 0, -1, GL_NONE, }

extern GLenum _wf_ge;
#define gl_error ((_wf_ge = glGetError()) != GL_NO_ERROR)
#define gl_warn(A, ...) { \
		if(gl_error){ \
		agl_print_error(__func__, _wf_ge, A, ##__VA_ARGS__); \
	}}

#define AGL_NEW(T, ...) ({T* obj = g_new0(T, 1); *obj = (T){__VA_ARGS__}; obj;})
