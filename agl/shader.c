/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/

#define TEXTURE_UNIT 0 // GL_TEXTURE0

static void _alphamap_set_uniforms       (AGlShader*);
static void _alphamap_split_set_uniforms (AGlShader*);
static void _tex_set_uniforms            (AGlShader*);
static void _v_scrollbar_set_uniforms    (AGlShader*);

void
agl_set_uniforms (AGlShader* shader)
{
	AGlUniformInfo* uniform = NULL;
	for (int i = 0; (uniform = &shader->uniforms[i]) && uniform->type; i++) {
		AGlUniformUnion* u = (AGlUniformUnion*)uniform;
		switch (uniform->type) {
			case GL_FLOAT:
				switch (uniform->size) {
					case 1:
						if (uniform->value[0] != uniform->state[0]) {
							glUniform1f(uniform->location, uniform->value[0]);
							uniform->state[0] = uniform->value[0];
						}
						break;
					case 2:
						if (uniform->value[0] != uniform->state[0] && uniform->value[1] != uniform->state[1]) {
							glUniform2fv(uniform->location, 1, uniform->value);
							uniform->state[0] = uniform->value[0];
							uniform->state[1] = uniform->value[1];
						}
						break;
				}
				break;
			case GL_INT:
				switch (uniform->size) {
					case 1:
						if (uniform->value[0] != uniform->state[0]) {
							glUniform1i(uniform->location, uniform->value[0]);
							uniform->state[0] = uniform->value[0];
						}
						break;
				}
				break;
			case GL_COLOR_ARRAY:
				agl_set_colour_uniform (uniform, u->value.i[0]);
				break;
		}
	}
}


static AlphaMapShader alphamap = {{
	NULL, NULL, 0,
	(AGlUniformInfo[]) {
	   {"tex2d", 1, GL_INT, -1, {TEXTURE_UNIT,}},
	   {"fg_colour", 4, GL_FLOAT, -1,},
	   END_OF_UNIFORMS
	},
	_alphamap_set_uniforms,
	&alpha_map_text
}};


static void
_alphamap_set_uniforms (AGlShader* shader)
{
	agl_set_colour_uniform (&alphamap.shader.uniforms[1], alphamap.uniform.fg_colour);
}


static AlphaMapShader alphamap_split = {{
	NULL, NULL, 0,
	(AGlUniformInfo[]) {
		{"tex2d", 1, GL_INT, -1, {TEXTURE_UNIT,}},
		{"fg_colour", 4, GL_FLOAT, -1,},
		END_OF_UNIFORMS
	},
	_alphamap_split_set_uniforms,
	&alpha_map_split_text
}};


static void
_alphamap_split_set_uniforms (AGlShader* shader)
{
	agl_set_colour_uniform (&alphamap_split.shader.uniforms[1], alphamap_split.uniform.fg_colour);
}


/*
 *  plain 2d texture
 */
static AlphaMapShader tex2d = {{
	NULL, NULL, 0,
	(AGlUniformInfo[]) {
	   {"tex2d", 1, GL_INT, -1, {TEXTURE_UNIT,}},
	   {"fg_colour", 4, GL_FLOAT, -1,},
	   END_OF_UNIFORMS
	},
	_tex_set_uniforms,
	&texture_2d_text
}};


static void
_tex_set_uniforms (AGlShader* shader)
{
	agl_set_colour_uniform (&tex2d.shader.uniforms[1], tex2d.uniform.fg_colour);
}


/*
 *  Plain colour shader
 */
AGlShader plain = {
	NULL, NULL, 0,
	(AGlUniformInfo[]) {
	   {"colour", 4, GL_COLOR_ARRAY, -1,},
	   END_OF_UNIFORMS
	},
	agl_set_uniforms,
	&plain_colour_text
};


AGlShader dotted = {
	NULL, NULL, 0,
	(AGlUniformInfo[]) {
	   {"colour", 4, GL_COLOR_ARRAY, -1,},
	   END_OF_UNIFORMS
	},
	agl_set_uniforms,
	&dotted_text
};


AGlShader rotatable = {
	NULL, NULL, 0,
	(AGlUniformInfo[]) {
	   {"tex2d", 1, GL_INT, -1, {TEXTURE_UNIT,}},
	   {"colour", 4, GL_COLOR_ARRAY, -1},
	   {"rotation", 1, GL_FLOAT, -1,},
	   {"origin", 2, GL_FLOAT, -1, {0,}},
	   END_OF_UNIFORMS
	},
	agl_set_uniforms,
	&rotatable_text};


ScrollbarShader v_scrollbar_shader = {{
	.uniforms = (AGlUniformInfo[]) {
	   {"colour", 4, GL_COLOR_ARRAY, -1,},
	   {"bg_colour", 4, GL_COLOR_ARRAY, -1,},
	   END_OF_UNIFORMS
	},
	_v_scrollbar_set_uniforms,
	&vscrollbar_text
}};


static void
_scrollbar_set_uniforms (ScrollbarShader* scrollbar_shader)
{
	#define CENTRE1 2
	#define CENTRE2 3
	#define RADIUS 4

	float centre1[2] = {scrollbar_shader->uniform.centre1.x, scrollbar_shader->uniform.centre1.y};
	float centre2[2] = {scrollbar_shader->uniform.centre2.x, scrollbar_shader->uniform.centre2.y};
	glUniform2fv(CENTRE1, 1, centre1);
	glUniform2fv(CENTRE2, 1, centre2);
	glUniform1f(RADIUS, scrollbar_shader->uniform.radius);

	agl_set_uniforms ((AGlShader*)scrollbar_shader);
}


static void
_v_scrollbar_set_uniforms (AGlShader* shader)
{
	_scrollbar_set_uniforms (&v_scrollbar_shader);
}


