/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#ifndef __agl_debug_h__
#define __agl_debug_h__

#include "agl/utils.h"
#include "debug/debug.h"

typedef enum
{
  AGL_DEBUG_GLYPH_CACHE = 1,
  AGL_DEBUG_SHADERS = 1 << 1,
  AGL_DEBUG_ALL     = 1 << 2,

} AGlDebugFlags;

#ifdef DEBUG
#  define AGL_DEBUG if(agl->debug)
#  define dbg2(FLAG, A, STR, ...) ({if(A <= _debug_ && (AGL_DEBUG_ ## FLAG & agl_get_instance()->debug_flags)){ fputs(__func__, stdout); printf(": "STR"\n", ##__VA_ARGS__);}})
#else
#  define AGL_DEBUG if(false)
#  define dbg2(FLAG, A, STR, ...)
#endif

#define AGL_DEBUG_CHECK(T) (agl_get_instance()->debug_flags & AGL_DEBUG_ ## T)

#endif
