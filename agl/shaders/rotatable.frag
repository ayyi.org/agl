uniform sampler2D tex2d;
uniform vec4 colour;

varying vec2 tex_coord;

void main (void)
{
   gl_FragColor = colour * texture2D(tex2d, tex_coord);
}
