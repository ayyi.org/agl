The Ayyi Graphics Library (AGL) provides an OpenGL-3 scene graph for 2D
applications on X11 with Gtk+ and GLX. A typical use-case would be a media
application that uses mostly custom elements. The goal is to be able to produce
highly fluid interfaces with high performance and low resource usage.

  * Nodes in the tree are rendered to FBO textures to provide caching.

  * Node functionality can be added using inheritance or behaviours.

  * Layout is done in the layout hook, and behaviours can be
    used for common patterns.

  * Layouts can be specified using YAML markup.

  * Change propagation is handled in large part with Observable's.

  * Pango is used for text rendering using texture atlases.

The library should be considered low-level, and because the
focus is on performance, very few concessions have
been made for ease-of-use or productivity.

AGL is similar in some ways to GSK in Gtk+ 4.
