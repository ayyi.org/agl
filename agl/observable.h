/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://www.ayyi.org           |
* | copyright (C) 2018-2021 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#ifndef __agl_observable_h__
#define __agl_observable_h__

typedef union
{
    int         i;
    float       f;
    double      d;
    char*       c;
    int64_t     b;
    void*       p;
} AGlVal;

typedef struct {
   AGlVal value;
   AGlVal min;
   AGlVal max;
   GList* subscriptions;
} AGlObservable;

typedef void (*AGlObservableFn) (AGlObservable*, AGlVal value, gpointer);

AGlObservable* agl_observable_new       ();
void           agl_observable_free      (AGlObservable*);
void           agl_observable_set_int   (AGlObservable*, int);
void           agl_observable_set_float (AGlObservable*, float);
void           agl_observable_subscribe (AGlObservable*, AGlObservableFn, gpointer);
void           agl_observable_subscribe_with_state
                                        (AGlObservable*, AGlObservableFn, gpointer);
void           agl_observable_unsubscribe
                                        (AGlObservable*, AGlObservableFn, gpointer);

#endif
