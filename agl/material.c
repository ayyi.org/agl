/*
 +----------------------------------------------------------------------+
 | This file is part of the Ayyi project. http://www.ayyi.org           |
 | copyright (C) 2013-2021 Tim Orford <tim@orford.org>                  |
 +----------------------------------------------------------------------+
 | This program is free software; you can redistribute it and/or modify |
 | it under the terms of the GNU General Public License version 3       |
 | as published by the Free Software Foundation.                        |
 +----------------------------------------------------------------------+
 |
 */

#include "config.h"
#include <math.h>
#include <assert.h>
#include <glib.h>
#include "debug/debug.h"
#include "agl/ext.h"
#include "agl/shader.h"
#include "agl/utils.h"

static AGl* agl = NULL;

static void agl_create_line_texture ();

AGlMaterialClass aaline_class = {.init = agl_create_line_texture};


void
agl_use_material (AGlMaterial* material)
{
	agl_use_program(material->material_class->shader);

	if (material->material_class->texture) {
		agl_enable(AGL_ENABLE_BLEND);
		agl_use_texture(aaline_class.texture);
	}
}


AGlMaterial*
agl_aa_line_new ()
{
	aaline_class.init();

	return AGL_NEW(AGlMaterial,
		.material_class = &aaline_class
	);
}


void
agl_aa_line_set_line (AGlTQuad (*v)[], int i, AGlfPt p0, AGlfPt p1)
{
	float len = sqrtf(powf((p1.y - p0.y), 2.) + powf((p1.x - p0.x), 2.));
	float xoff = (p1.y - p0.y) * 5. / len / 4.;
	float yoff = (p1.x - p0.x) * 5. / len / 4.;

	(*v)[i] = (AGlTQuad){
		{(AGlVertex){p0.x - xoff, p0.y + yoff}, (AGlVertex){0.0, 0.0}},
		{(AGlVertex){p1.x - xoff, p1.y + yoff}, (AGlVertex){1.0, 0.0}},
		{(AGlVertex){p1.x + xoff, p1.y - yoff}, (AGlVertex){1.0, 1.0}},
		{(AGlVertex){p0.x - xoff, p0.y + yoff}, (AGlVertex){0.0, 0.0}},
		{(AGlVertex){p1.x + xoff, p1.y - yoff}, (AGlVertex){1.0, 1.0}},
		{(AGlVertex){p0.x + xoff, p0.y - yoff}, (AGlVertex){0.0, 1.0}}
	};
}


static void
agl_create_line_texture ()
{
	if (aaline_class.texture) return;

	agl = agl_get_instance();

	aaline_class.shader = (AGlShader*)agl->shaders.alphamap;

#if 1
	int width = 4;
	int height = 5;
	char* pbuf = g_new0(char, width * height);
	int y;
	//char vals[] = {0xff, 0xa0, 0x40};
	char vals[] = {0xff, 0x40, 0x10};
	for (int x=0;x<width;x++) {
		y=0; *(pbuf + y * width + x) = vals[2];
		y=1; *(pbuf + y * width + x) = vals[1];
		y=2; *(pbuf + y * width + x) = vals[0];
		y=3; *(pbuf + y * width + x) = vals[1];
		y=4; *(pbuf + y * width + x) = vals[2];
	}
#else
	int width = 4;
	int height = 4;
	char* pbuf = g_new0(char, width * height);
	int y; for(y=0;y<height/2;y++){
		int x; for(x=0;x<width;x++){
			*(pbuf + y * width + x) = 0xff * (2*y)/height + 128;
			*(pbuf + (height -1 - y) * width + x) = 0xff * (2*y)/height + 128;
		}
	}
#endif

	glGenTextures(1, &aaline_class.texture);
	gl_warn("could not create line_texture");
	dbg(2, "line_texture=%u", aaline_class.texture);

	#define pixel_format GL_ALPHA

	agl_use_texture (aaline_class.texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, pixel_format, GL_UNSIGNED_BYTE, pbuf);
	gl_warn("error binding line texture");

	g_free(pbuf);
}

