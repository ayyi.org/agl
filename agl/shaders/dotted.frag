/*
  copyright (C) 2021 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

uniform vec4 colour;

varying vec2 MCposition;

void main ()
{
	// TODO make it work for horizontal lines too

	float interval = 2.0;
	float m = mod(MCposition.x, interval);
	float n = mod(MCposition.y, interval);

	if(fract(MCposition.y / 3.0) > 0.6)
		gl_FragColor = colour;
	else
		discard;
}

