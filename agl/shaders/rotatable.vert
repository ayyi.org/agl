/*
  copyright (C) 2021 Tim Orford <tim@orford.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 3
  as published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

attribute vec4 vertex;

uniform vec2 modelview;
uniform vec2 translate;

uniform float rotation;
uniform vec2 origin;

varying vec2 tex_coord;


vec2 rotate (vec2 v, float a)
{
   float s = sin(a);
   float c = cos(a);
   mat2 m = mat2(c, -s, s, c);

   return m * v;
}

void main ()
{
	tex_coord = vertex.zw;

	gl_Position = vec4(
		vec2(1., -1.) * (rotate(vertex.xy - origin, rotation) + origin + translate) / modelview - vec2(1., -1.),
		0.,
		1.
	);
}
