uniform vec2 modelview;
uniform vec2 translate;

uniform float u_alpha;

attribute vec2 aPosition;
attribute vec2 aUv;
varying vec2 vUv;
